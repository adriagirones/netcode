﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

[CreateAssetMenu]
public class so_savePipeline : ScriptableObject
{
    public UniversalRenderPipelineAsset pipeline;
    [HideInInspector]
    public bool hdrActivated;
    [HideInInspector]
    public int antiAliassing;
    [HideInInspector]
    public int resolutionWidth;
    [HideInInspector]
    public int resolutionHeight;
    [HideInInspector]
    public bool fullscreen;
}
