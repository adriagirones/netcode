﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_UnlockedNodes : ScriptableObject
{
    public so_Nodes[] atkNodes;
    public so_Nodes[] hpNodes;
    public so_Nodes[] mpNodes;
    public so_Nodes[] comboNodes;
    public so_Nodes[] skillWindNodes;
    public so_Nodes[] skillIceNodes;
    public so_Nodes[] skillFireNodes;
    public so_Nodes[] skillElectNodes;

    public float cooldownWindSkill;
    public float cooldownFireSkill;
    public float cooldownIceSkill;
    public float cooldownElectSkill;

    public int damageWindSkill;
    public int damageFireSkill;
    public int damageIceSkill;
    public int damageElectSkill;
    public int damageElectSkillBall;

    public int getUnlockedAtkNodes()
    {
        int unlockedNodes = 0;
        foreach (so_Nodes node in atkNodes)
            if (node.activated) unlockedNodes++;
        return unlockedNodes;
    }

    public int getUnlockedHpNodes()
    {
        int unlockedNodes = 0;
        foreach (so_Nodes node in hpNodes)
            if (node.activated) unlockedNodes++;
        return unlockedNodes;
    }

    public int getUnlockedMpNodes()
    {
        int unlockedNodes = 0;
        foreach (so_Nodes node in mpNodes)
            if (node.activated) unlockedNodes++;
        return unlockedNodes;
    }

    public int getUnlockedComboNodes()
    {
        int unlockedNodes = 0;
        foreach (so_Nodes node in comboNodes)
            if (node.activated) unlockedNodes++;
        return unlockedNodes;
    }

    public int getUnlockedWindNodes()
    {
        int unlockedNodes = 0;
        foreach (so_Nodes node in skillWindNodes)
            if (node.activated) unlockedNodes++;
        return unlockedNodes;
    }

    public int getUnlockedFireNodes()
    {
        int unlockedNodes = 0;
        foreach (so_Nodes node in skillFireNodes)
            if (node.activated) unlockedNodes++;
        return unlockedNodes;
    }

    public int getUnlockedElectNodes()
    {
        int unlockedNodes = 0;
        foreach (so_Nodes node in skillElectNodes)
            if (node.activated) unlockedNodes++;
        return unlockedNodes;
    }

    public int getUnlockedIceNodes()
    {
        int unlockedNodes = 0;
        foreach (so_Nodes node in skillIceNodes)
            if (node.activated) unlockedNodes++;
        return unlockedNodes;
    }

    public int getDamageWindSkill()
    {
        return damageWindSkill * getUnlockedWindNodes();
    }

    public int getDamageFireSkill()
    {
        return damageFireSkill * getUnlockedFireNodes();
    }

    public int getDamageIceSkill()
    {
        return damageIceSkill * getUnlockedIceNodes();
    }

    public int getDamageElectSkill()
    {
        return damageElectSkill;
    }

    public int getDamageElectSkillBall()
    {
        return damageElectSkillBall;
    }

}
