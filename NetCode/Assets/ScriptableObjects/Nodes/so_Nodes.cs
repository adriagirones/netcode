﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class so_Nodes : ScriptableObject
{
    public bool activated;
    public int cost;
    public string title;
    public string description;
}
