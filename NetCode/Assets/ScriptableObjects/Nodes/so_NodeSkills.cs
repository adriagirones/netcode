﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_NodeSkills : so_Nodes
{
    public enum stat
    {
        WIND,
        FIRE,
        ELEC,
        ICE,
        COMBO
    }

    public stat type;
    public int damage;
}
