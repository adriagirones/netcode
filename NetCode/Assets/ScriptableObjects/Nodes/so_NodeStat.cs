﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_NodeStat : so_Nodes
{
    public enum stat
    {
        ATK,
        MAXHP,
        MAXMP
    }

    public stat type;
    public int value;
}
