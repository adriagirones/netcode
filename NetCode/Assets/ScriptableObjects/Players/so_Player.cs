﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Player : ScriptableObject
{
    public string nameCharacter;
    public float hp;
    public float maxhp;
    public int podatk;
    public int atk;
    public int xp;
    public int lvl;
    public int hpLvl1;
    public int podAtkLvl1;
    public int atkLvl1;
    public int xpNextLevel;
    public int skillPoints;

    public so_Weapon actualWeapon;

    public so_UnlockedNodes skillsUnlocked;

    public string positionLastCheckpoint;

    public void upExp(int n)
    {
            xp += n;
            if (xp >= xpNextLevel)
            {
                int a = xp - xpNextLevel;
                xp = a;
                levelUp();
            }
    }
    public void levelUp()
    {
        maxhp = (int)(hpLvl1 * ((lvl * 0.1f) + 1));
       // podatk = (int)(podAtkLvl1 * ((lvl * 0.1f) + 1));
        atk = (int)(atkLvl1 * ((lvl * 0.1f) + 1));
        lvl++;
        if (lvl == 10)
        {
            xpNextLevel = 0;
            xp = 0;
        }
        else
        {
            xpNextLevel *= 2;
            //hp = getGlobalMaxHP();
            podatk = getGlobalAtkPod();
        }
    }

    public float getGlobalMaxHP()
    {
        float var = maxhp;
        foreach (so_NodeStat node in skillsUnlocked.hpNodes)
            if (node.activated) var += node.value;
        return var;
    }

    public int getGlobalAtkPod()
    {
        int var = podatk;
        foreach (so_NodeStat node in skillsUnlocked.mpNodes)
            if (node.activated) var += node.value;
        return var;
    }

    public int getGlobalAtk()
    {
        int var = atk;
        foreach(so_NodeStat node in skillsUnlocked.atkNodes)
            if (node.activated) var += node.value;

        return var;
    }
}
