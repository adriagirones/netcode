﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Weapon : so_EquipableObjects
{
    public bool isEnabled;
    public bool isActived;
    public int cost;
    public enum TypeWeapon
    {
        WEAPON1,
        WEAPON2
    }

    public TypeWeapon type;
}
