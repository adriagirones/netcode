﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Enemies : ScriptableObject
{
    public int maxHp;
    public int atk;
    public float cadency;
    public float speed;

    public int nbullets;
    public float bulletspread;
    public float bulletspeed;
    public float bulletLspeed;
    public float bulletLfreq;
    public float bulletlifetime;
    public float canonangle;

}
