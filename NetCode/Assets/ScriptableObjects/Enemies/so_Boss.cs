﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Boss : ScriptableObject
{
    public float maxarmhp;
    public float maxcanonhp;
    public float generalhp;
    public float overheat;
}
