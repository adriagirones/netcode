﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class so_Checkpoint : ScriptableObject
{
    public bool isActivated;
    public string sceneName;
    public Transform spawnPoint;
    public string nameGameObject;
}
