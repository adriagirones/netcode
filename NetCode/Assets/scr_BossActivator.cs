﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_BossActivator : MonoBehaviour
{
    public GameObject[] boss;
    public GameObject[] hpBars;

    private void activateBoss()
    {
        foreach (GameObject go in boss)
            go.SetActive(true);

        foreach (GameObject go in hpBars)
            go.SetActive(true);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            activateBoss();
            gameObject.SetActive(false);
        }
    }

}
