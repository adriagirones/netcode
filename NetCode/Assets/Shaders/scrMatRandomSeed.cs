//using System;
//using UnityEngine;
//using NaughtyAttributes;
//using Random = UnityEngine.Random;

//public class scrMatRandomSeed : MonoBehaviour
//{
//    private MaterialPropertyBlock properties;
//    [SerializeField]
//    private ShaderProperty[] shadProps;

//    void Start()
//    {
//        properties = new MaterialPropertyBlock();
//        MeshRenderer rend = GetComponent<MeshRenderer>();

//        foreach (ShaderProperty t in shadProps)
//        {
//            rend.GetPropertyBlock(properties);
//            properties.SetFloat(t.varName, Random.Range(t.randomRange.x, t.randomRange.y));
//            rend.SetPropertyBlock(properties);
//        }
//    }
//}

//[Serializable]
//class ShaderProperty
//{
//    public string varName;
//    [MinMaxSlider(-10, 10)]
//    public Vector2 randomRange;
//}
