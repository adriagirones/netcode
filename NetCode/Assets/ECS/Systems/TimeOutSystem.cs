﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;

public class TimeOutSystem : SystemBase
{
    EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    protected override void OnUpdate()
    {
        var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();
        float3 PlayerPos = scr_PlayerController.actualPosition;
        float deltaT = Time.DeltaTime;

        NativeArray<int> result = new NativeArray<int>(1, Allocator.TempJob);
        NativeArray<int> result2 = new NativeArray<int>(1, Allocator.TempJob);

        Entities.WithAny<BulletTag>().ForEach((Entity bullet, int entityInQueryIndex, ref LifeTimeComponent lifetime, in Translation trans) =>
        {
            if (math.distance(PlayerPos, trans.Value) < 1f )
            {
                commandBuffer.DestroyEntity(entityInQueryIndex, bullet);
                result[0] = 1;
            }

            if (lifetime.ActualTime > lifetime.TimeLeft)
            {
                commandBuffer.DestroyEntity(entityInQueryIndex, bullet);
            }
            else
            {
                lifetime.ActualTime += deltaT;
            }

        }).WithName("destroybulletjob").ScheduleParallel();

        this.CompleteDependency();
        if (result[0] == 1)
        {
            scr_PlayerController.instance.HittedBullet(false);
            result[0] = 0;
        }
        result.Dispose();

        Entities.WithAny<RandomBulletTag>().ForEach((Entity bullet, int entityInQueryIndex, ref LifeTimeComponent lifetime, in Translation trans) =>
        {
            if (math.distance(PlayerPos, trans.Value) < 1f)
            {
                commandBuffer.DestroyEntity(entityInQueryIndex, bullet);
                result2[0] = 1;
            }
            if (lifetime.ActualTime > lifetime.TimeLeft)
            {
                commandBuffer.DestroyEntity(entityInQueryIndex, bullet);
            }
            else
            {
                lifetime.ActualTime += deltaT;
            }

        }).WithName("destroyrandombulletjob").ScheduleParallel();

        this.CompleteDependency();
        if (result2[0] == 1)
        {
            scr_PlayerController.instance.HittedBullet(false);
            result2[0] = 0;
        }
        result2.Dispose();

        barrier.AddJobHandleForProducer(Dependency);
    }


}