﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class MoveForwardSystem : SystemBase
{
    EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    protected override void OnUpdate()
    {
        var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();
        float deltaT = Time.DeltaTime;
        float elapsedT = (float)Time.ElapsedTime;
        bool world = scr_WorldManager.instance.getIsInWorldBlue();

        Entities.WithAny<BulletTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation trans, ref Rotation rot, in SpeedComponent speeds) =>
        {
            float auxTime = elapsedT + entityInQueryIndex * math.PI;
            if(world)
                trans.Value += speeds.ForwardSpeed * deltaT * math.forward(rot.Value);
            else
                trans.Value += (speeds.ForwardSpeed / 3) * deltaT * math.forward(rot.Value);
        }).ScheduleParallel();

        Entities.WithAny<RandomBulletTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation trans, ref Rotation rot, in SpeedComponent speeds) =>
        {
            float auxTime = elapsedT + entityInQueryIndex * math.PI;
            float auxSpeed;
            if (world)
                auxSpeed = speeds.ForwardSpeed + math.clamp(math.sin(entityInQueryIndex), 0, 2);
            else
                auxSpeed = (speeds.ForwardSpeed / 3) + math.clamp(math.sin(entityInQueryIndex), 0, 2);

            trans.Value += auxSpeed * deltaT * math.forward(rot.Value);
            trans.Value += speeds.LateralSpeed * math.sin(auxTime * (math.PI * 2) * speeds.LateralFrequency) * deltaT 
                    * math.mul(Quaternion.Euler(0f, 90f, 0f), math.forward(rot.Value));
        }).ScheduleParallel();

    }
}
