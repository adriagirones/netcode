﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct SpeedComponent : IComponentData
{
    public float ForwardSpeed;
    public float AngularSpeed;
    public float LateralSpeed;
    public float LateralFrequency;
}
