﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using UnityEngine.Jobs;
using Unity.Transforms;
using UnityEngine.Events;

using math = Unity.Mathematics.math;
using random = Unity.Mathematics.Random;

public class scr_DaniBulletManager : MonoBehaviour
{

    [Header("References")]
    private bool turn = false;
    private bool turn2 = false;
    private bool turn3 = false;
    public float turnspeed;
    private float auxTime;


    private EntityManager entityManager;

    [SerializeField] private GameObject bulletprefab;
    [SerializeField] private GameObject randombulletprefab;
    private Entity bullet;
    private Entity randombullet;

    public float waitTime;
    //private JobHandle shootjobhandle;
    //private ShootJob kashootjob;

    private bool stopShoot = true;
    private bool initShooting = false;
    public bool debugmode;
    public GameObject mainPJ;

    Coroutine shootCorroutine = null;


    public enum ShootModes
    {
        mode1,
        mode2,
        mode3,
        paused
    }

    public ShootModes shootmode;


    public GameEvent Event;

    // Start is called before the first frame update
    /*void Start()
    {
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        bullet = GameObjectConversionUtility.ConvertGameObjectHierarchy(bulletprefab, settings);
        randombullet = GameObjectConversionUtility.ConvertGameObjectHierarchy(randombulletprefab, settings);
        StartCoroutine(Shooting());
        //Shoot();
    }*/

    private void Start()
    {
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        bullet = GameObjectConversionUtility.ConvertGameObjectHierarchy(bulletprefab, settings);
        randombullet = GameObjectConversionUtility.ConvertGameObjectHierarchy(randombulletprefab, settings);
    }

    public void startShooting(so_Enemies myEnemySO, Transform myEnemy)
    {
        Shoot(myEnemySO, myEnemy);
    }

    public void stopShooting(so_Enemies myEnemySO, Transform myEnemy)
    {
        //Shoot(myEnemySO, myEnemy);
    }

    /*private IEnumerator Shooting()
    {
        float acc = 0;
        float acc2 = 0;
        int shootmodeacc = 0;
        bool debugbool = false;
        shootmode = ShootModes.mode3;
        while (stopShoot)
        {
            if (debugmode)
            {
                switch (shootmode)
                {
                    case ShootModes.mode1:
                        turn = true;
                        turn2 = false;
                        turn3 = false;
                        if (!debugbool)
                        {
                            auxTime = Time.time;
                            debugbool = true;
                        }
                        break;
                    case ShootModes.mode2:
                        turn = false;
                        turn2 = true;
                        turn3 = false;
                        debugbool = false;
                        break;
                    case ShootModes.mode3:
                        turn = false;
                        turn2 = false;
                        turn3 = true;
                        debugbool = false;
                        break;
                    case ShootModes.paused:
                        turn = false;
                        turn2 = false;
                        turn3 = false;
                        debugbool = false;
                        break;
                }
            }
            else
            {

               // print(acc + " " + acc2);
                if (acc2 > 0.4)
                {
                    shootmodeacc++;
                    if (shootmodeacc % 2 == 0)
                        shootmode = ShootModes.mode3;
                    else
                    {
                        //shootmode = (ShootModes)(int)Random.Range(0, 2);
                        shootmode = ShootModes.mode3;
                    }
                    switch (shootmode)
                    {
                        case ShootModes.mode1:
                            turn = true;
                            turn2 = false;
                            auxTime = Time.time;
                            break;
                        case ShootModes.mode2:
                            turn = false;
                            turn2 = true;
                            break;
                        case ShootModes.mode3:
                            turn = false;
                            turn2 = false;
                            turn3 = true;
                            break;
                        case ShootModes.paused:
                            turn = false;
                            turn2 = false;
                            turn3 = false;
                            break;
                    }
                    print("LETS TURN");
                    //if (turn2)
                    //{
                    //    if (turn)
                    //    {
                    //        turn = false;
                    //    }
                    //    else
                    //    {

                    //        turn = true;
                    //        auxTime = Time.time;
                    //    }
                    //}

                    acc2 = 0;
                }
            }

            if (acc > 0.2)
            {
                acc = 0;
                Shoot(true);
                // Shoot(false);
                print("RANDOMSHOOT");
            }
            else
            {
                Shoot(false);
                print("SHOOT");
            }

            yield return new WaitForSeconds(waitTime);
            acc += Time.deltaTime;
            acc2 += Time.deltaTime;

        }
    }*/

    private void Shoot(so_Enemies myEnemySO, Transform myEnemy)
    {
        float auxangle = myEnemySO.bulletspread / myEnemySO.nbullets;
        float startangle = (myEnemySO.bulletspread / 2) * -1;

        float finalrot = myEnemy.rotation.eulerAngles.y + startangle;
        NativeArray<Entity> bulletArray = new NativeArray<Entity>(myEnemySO.nbullets, Allocator.Temp);
        for (int i = 0; i < myEnemySO.nbullets; i++)
        {
            finalrot += auxangle;
            Quaternion aux = Quaternion.Euler(myEnemy.rotation.eulerAngles.x, finalrot, myEnemy.rotation.eulerAngles.z);
            bulletArray[i] = entityManager.Instantiate(bullet);
            entityManager.SetComponentData(bulletArray[i], new Translation { Value = myEnemy.position });
            entityManager.SetComponentData(bulletArray[i], new Rotation { Value = aux });
            entityManager.SetComponentData(bulletArray[i], new SpeedComponent { ForwardSpeed = myEnemySO.bulletspeed, LateralSpeed = myEnemySO.bulletLspeed, LateralFrequency = myEnemySO.bulletLfreq });
            entityManager.SetComponentData(bulletArray[i], new LifeTimeComponent { TimeLeft = myEnemySO.bulletlifetime, ActualTime = 0f });

        }

        bulletArray.Dispose();

    }


    //private struct ShootJob: IJobParallelFor
    //{
    //    public int nbullets;
    //    public float bulletspread;
    //    public float bulletspeed;
    //    public float bulletLspeed;
    //    public float bulletLfreq;
    //    public float bulletlifetime;
    //    public Transform canon;
    //    public Entity bullet;

    //    private NativeArray<Entity> bulletArray;

    //    public void Execute(int i)
    //    {

    //    }

    //}


    // Update is called once per frame
    void Update()
    {
        /*if (turn)
        {
            Quaternion rotaux = Quaternion.Euler(0f, turnspeed * (Time.time - auxTime) + 90f, 0f);

            canon.rotation = Quaternion.Lerp(canon.rotation, rotaux, .2f);
            print(Time.time - auxTime);
            //canon.Rotate(0f, turnspeed * Time.deltaTime, 0f);
        }
        else if (turn2)
        {
            float t = (Mathf.Sin(Time.time) / 2) + .5f;

            //Quaternion rot1 = Quaternion.Euler(0f, canonangle + 90f, 0f);
            //Quaternion rot2 = Quaternion.Euler(0f, -canonangle + 90f, 0f);

            //canon.rotation = Quaternion.Lerp(rot1, rot2, t);
        }
        else if (turn3)
        {
            canon.LookAt(mainPJ.transform);
        }
        else
        {
            canon.rotation = Quaternion.Lerp(canon.rotation, Quaternion.Euler(0f, 90f, 0f), .05f);
        }

        /* (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot(false);
            //kashootjob = new ShootJob()
            //{
            //    nbullets = this.nbullets,
            //    bulletspread = this.bulletspread,
            //    bulletspeed = this.bulletspeed,
            //    bulletLspeed = this.bulletLspeed,
            //    bulletLfreq = this.bulletLfreq,
            //    bulletlifetime = this.bulletlifetime,
            //    canon = this.canon,
            //    bullet = this.bullet
            //};
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (!stopShoot)
            {

                stopShoot = !stopShoot;
                StartCoroutine(Shooting());
            }
            else
            {
                stopShoot = !stopShoot;
                StopCoroutine(Shooting());
            }


        }*/
    }
}


