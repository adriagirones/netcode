﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_AudioManager : MonoBehaviour
{
    public AudioSource bossdie;
    public AudioSource bosshitted;
    public AudioSource bossShoot;
    public AudioSource enemyFollowandBossState;
    public AudioSource powerOnandStartFollow;
    public AudioSource powerOnBoss;
    public AudioSource robotsound;
    public AudioSource robothit;
    public AudioSource shoot2;
    public AudioSource flameAround;
    public AudioSource frontflame;

    public AudioSource flameskill;
    public AudioSource iceskill;
    public AudioSource thunderskill;
    public AudioSource thunderskill2;
    public AudioSource tornado;

    public AudioSource swordslash;

    public AudioSource uibutton;

    public AudioSource backgroundmusic;
    public AudioSource backgroundmusicinverted;
    public AudioSource intromusic;

    public static scr_AudioManager instance;

    private void Awake()
    {
        instance = this;
    }

    public enum sound
    {
        bossdie,
        bosshitted,
        bossShoot,
        enemyFollowandBossState,
        powerOnandStartFollow,
        powerOnBoss,
        robotsound,
        robothit,
        shoot2,
        flameAround,
        frontflame,
        flameskill,
        iceskill,
        thunderskill,
        thunderskill2,
        tornado,
        swordslash,
        uibutton,
        backgroundmusic,
        backgroundmusicinverted,
        intromusic
    }

    public void playSound(sound toplay)
    {
        switch (toplay)
        {
            case sound.bossdie:
                if(!bossdie.isPlaying)
                    bossdie.Play();
                break;
            case sound.bosshitted:
                if (!bosshitted.isPlaying)
                    bosshitted.Play();
                break;
            case sound.bossShoot:
                bossShoot.Play();
                break;
            case sound.enemyFollowandBossState:
                if (!enemyFollowandBossState.isPlaying)
                    enemyFollowandBossState.Play();
                break;
            case sound.powerOnandStartFollow:
                if (!powerOnandStartFollow.isPlaying)
                    powerOnandStartFollow.Play();
                break;
            case sound.powerOnBoss:
                if (!powerOnBoss.isPlaying)
                    powerOnBoss.Play();
                break;
            case sound.robotsound:
                if (!robotsound.isPlaying)
                    robotsound.Play();
                break;
            case sound.robothit:
                if (!robothit.isPlaying)
                    robothit.Play();
                break;
            case sound.shoot2:
                shoot2.Play();
                break;
            case sound.flameAround:
                flameAround.Play();
                break;
            case sound.frontflame:
                frontflame.Play();
                break;
            case sound.flameskill:
                flameskill.Play();
                break;
            case sound.iceskill:
                iceskill.Play();
                break;
            case sound.thunderskill:
                thunderskill.Play();
                break;
            case sound.thunderskill2:
                thunderskill2.Play();
                break;
            case sound.tornado:
                tornado.Play();
                break;
            case sound.swordslash:
                swordslash.Play();
                break;
            case sound.uibutton:
                uibutton.Play();
                break;
            case sound.backgroundmusic:
                backgroundmusic.Play();
                break;
            case sound.backgroundmusicinverted:
                backgroundmusicinverted.Play();
                break;
            case sound.intromusic:
                intromusic.Play();
                break;
        }
    }

    public void stopSound(sound toplay)
    {
        switch (toplay)
        {
            case sound.bossdie:
                if (bossdie.isPlaying)
                    bossdie.Stop();
                break;
            case sound.bosshitted:
                if (bosshitted.isPlaying)
                    bosshitted.Stop();
                break;
            case sound.bossShoot:
                bossShoot.Stop();
                break;
            case sound.enemyFollowandBossState:
                if (enemyFollowandBossState.isPlaying)
                    enemyFollowandBossState.Stop();
                break;
            case sound.powerOnandStartFollow:
                if (powerOnandStartFollow.isPlaying)
                    powerOnandStartFollow.Stop();
                break;
            case sound.powerOnBoss:
                if (powerOnBoss.isPlaying)
                    powerOnBoss.Stop();
                break;
            case sound.robotsound:
                if (robotsound.isPlaying)
                    robotsound.Stop();
                break;
            case sound.robothit:
                if (robothit.isPlaying)
                    robothit.Stop();
                break;
            case sound.shoot2:
                shoot2.Stop();
                break;
            case sound.flameAround:
                if (flameAround.isPlaying)
                    flameAround.Stop();
                break;
            case sound.frontflame:
                if (frontflame.isPlaying)
                    frontflame.Stop();
                break;
            case sound.flameskill:
                flameskill.Stop();
                break;
            case sound.iceskill:
                iceskill.Stop();
                break;
            case sound.thunderskill:
                thunderskill.Stop();
                break;
            case sound.thunderskill2:
                thunderskill2.Stop();
                break;
            case sound.tornado:
                tornado.Stop();
                break;
            case sound.swordslash:
                swordslash.Stop();
                break;
            case sound.uibutton:
                uibutton.Stop();
                break;
            case sound.backgroundmusic:
                backgroundmusic.Stop();
                break;
            case sound.backgroundmusicinverted:
                backgroundmusicinverted.Stop();
                break;
            case sound.intromusic:
                intromusic.Stop();
                break;
        }
    }
}
