﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class scr_SkillMenu : MonoBehaviour
{
    public GameObject fireskill;
    public GameObject elecskill;
    public GameObject iceskill;
    public GameObject windskill;

    public so_Player player;

    // Start is called before the first frame update
    void Start()
    {

    }
    private void OnEnable()
    {
        /*
        if (player.skillsUnlocked.getUnlockedFireNodes() > 0)
        {
            doAlpha(fireskill, 1f);
            if(fireskill.transform.GetChild(0).gameObject.activeSelf)
                fireskill.transform.GetChild(0).gameObject.SetActive(false);

            if(player.skillsUnlocked.getUnlockedFireNodes() == 2)
            {

            }
            else if(player.skillsUnlocked.getUnlockedFireNodes() == 1)
            {

            }


            setSkillValues(fireskill, player.skillsUnlocked.getUnlockedFireNodes(), (so_NodeSkills) player.skillsUnlocked.skillFireNodes[player.skillsUnlocked.getUnlockedFireNodes()-1]);
        }
        else
            doAlpha(fireskill,0.5f);

        if (player.skillsUnlocked.getUnlockedElectNodes() > 0)
        {
            doAlpha(elecskill, 1f);

            if (elecskill.transform.GetChild(0).gameObject.activeSelf)
                elecskill.transform.GetChild(0).gameObject.SetActive(false);
            setSkillValues(elecskill, player.skillsUnlocked.getUnlockedElectNodes(), (so_NodeSkills)player.skillsUnlocked.skillElectNodes[player.skillsUnlocked.getUnlockedElectNodes()-1]);
        }
        else
            doAlpha(elecskill, 0.5f);

        if (player.skillsUnlocked.getUnlockedIceNodes() > 0)
        {
            doAlpha(iceskill, 1f);
            if (iceskill.transform.GetChild(0).gameObject.activeSelf)
                iceskill.transform.GetChild(0).gameObject.SetActive(false);
            setSkillValues(iceskill, player.skillsUnlocked.getUnlockedIceNodes(), (so_NodeSkills)player.skillsUnlocked.skillIceNodes[player.skillsUnlocked.getUnlockedIceNodes()-1]);
        }
        else
            doAlpha(iceskill, 0.5f);

        if (player.skillsUnlocked.getUnlockedWindNodes() > 0)
        {
            doAlpha(windskill, 1f);
            if (windskill.transform.GetChild(0).gameObject.activeSelf)
                windskill.transform.GetChild(0).gameObject.SetActive(false);
            setSkillValues(windskill, player.skillsUnlocked.getUnlockedWindNodes(), (so_NodeSkills)player.skillsUnlocked.skillWindNodes[player.skillsUnlocked.getUnlockedWindNodes()-1]);
        }
        else
            doAlpha(windskill, 0.5f);*/

        enableDescriptions(fireskill, player.skillsUnlocked.getUnlockedFireNodes());
        enableDescriptions(elecskill, player.skillsUnlocked.getUnlockedElectNodes());
        enableDescriptions(iceskill, player.skillsUnlocked.getUnlockedIceNodes());
        enableDescriptions(windskill, player.skillsUnlocked.getUnlockedWindNodes());
    }

    private void doAlpha(GameObject skillgo,float alpha)
    {
        var auxcolor = skillgo.GetComponent<Image>().color;
        if(auxcolor.a != alpha)
        {
            auxcolor.a = alpha;
            skillgo.GetComponent<Image>().color = auxcolor;
            skillgo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().color = auxcolor;
            skillgo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().color = auxcolor;
            skillgo.transform.GetChild(3).GetComponent<TextMeshProUGUI>().color = auxcolor;
            skillgo.transform.GetChild(4).GetComponent<TextMeshProUGUI>().color = auxcolor;
        }
    }

    private void enableDescriptions(GameObject skillgo, int levelUnlocked)
    {
        if(levelUnlocked == 2)
        {
            print("es dos");
            skillgo.transform.GetChild(2).GetChild(1).gameObject.SetActive(false);
            skillgo.transform.GetChild(2).GetChild(2).gameObject.SetActive(true);
        }
        if (levelUnlocked >= 1)
        {
            skillgo.transform.GetChild(1).GetChild(1).gameObject.SetActive(false);
            skillgo.transform.GetChild(1).GetChild(2).gameObject.SetActive(true);
        }
        /*if(levelUnlocked == 0)
        {
            skillgo.transform.GetChild(1).GetChild(1).gameObject.SetActive(true);
            skillgo.transform.GetChild(1).GetChild(2).gameObject.SetActive(false);
        }*/
    }

    private void setSkillValues(GameObject skillgo, int level, so_NodeSkills skill)
    {
       // if(skillgo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text != "Level: " + level)
      //  {
      //      skillgo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Level: " + level;
            skillgo.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "Damage: " + skill.damage;
            skillgo.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = "Cooldown: " + skill.cost;
      //  }
    }

}
