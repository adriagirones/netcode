﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scr_ChangeScene : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
            StartCoroutine(LoadAsync());
    }

    IEnumerator LoadAsync()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("FinalBoss");

        while (!operation.isDone)
        {
            yield return null;
        }
        scr_PlayerController.instance.transform.position = scr_WorldManager.instance.getLastCheckpoint().position;
    }
}
