﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.VFX;
using UnityEngine.SceneManagement;

public class scr_PlayerController : MonoBehaviour
{
    private Camera mainCamera;
    private Rigidbody rb;
    private Animator anim;
    private scr_WorldManager worldManager;

    private bool DoingAction = false;
    private bool running = false;
    private int chain_attack_acc = 0;

    private Vector2 move;
    private Vector2 rotate;

    public GameObject weapon;
    public so_Weapon[] weaponsSO;
    public GameObject[] weapons;

    [HideInInspector]
    public float lerpTimerHPbar;

    public so_Player player;

    public InputController controls;
    public static scr_PlayerController instance;

    public Transform playercenter;
    private Transform hurtbox;

    private void Awake()
    {
        instance = this;
        worldManager = GameObject.Find("WorldManager").GetComponent<scr_WorldManager>();

        controls = new InputController();

        controls.Gameplay.Rotation.performed += cntxt => rotate = cntxt.ReadValue<Vector2>();
        controls.Gameplay.Rotation.canceled += cntxt => rotate = Vector2.zero;

        controls.Gameplay.Movement.performed += cntxt => move = cntxt.ReadValue<Vector2>();
        controls.Gameplay.Movement.canceled += cntxt => move = Vector2.zero;

        controls.Gameplay.Jump.performed += cntxt => Jump();

        controls.Gameplay.Dash.performed += cntxt => Dashing();

        controls.Gameplay.Run.performed += cntxt => Run();
        controls.Gameplay.Run.canceled += cntxt => StopRun();

        controls.Gameplay.ZoomCamera.performed += cntxt => zoom = cntxt.ReadValue<float>();
        controls.Gameplay.ZoomCamera.canceled += cntxt => zoom = 0f;
        controls.Gameplay.ZoomIn.performed += cntxt => ZoomIn();
        controls.Gameplay.ZoomOut.performed += cntxt => ZoomOut();
        controls.Gameplay.ZoomIn.canceled += cntxt => ZoomCancel();
        controls.Gameplay.ZoomOut.canceled += cntxt => ZoomCancel();

        controls.Gameplay.Attack.performed += cntxt => Attack();

        controls.Gameplay.FireSkill.performed += cntxt => FireSkill();
        controls.Gameplay.IceSkill.performed += cntxt => IceSkill();
        controls.Gameplay.ElectroSkill.performed += cntxt => ElectroSkill();
        controls.Gameplay.WindSkill.performed += cntxt => WindSkill();

        controls.Gameplay.OpenMenu.performed += cntxt => OpenMainMenu();

        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        mainCamera = Camera.main;
        hurtbox = transform.Find("Hurtbox");

        //if (player.positionLastCheckpoint != "" && GameObject.Find(player.positionLastCheckpoint) != null)
        if (SceneManager.GetActiveScene().name == "Level1")
        {
            transform.position = scr_WorldManager.instance.getLastCheckpoint().position;
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.backgroundmusic);
        }
        else if (SceneManager.GetActiveScene().name == "FinalBoss")
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.backgroundmusicinverted);

        for (int i = 0; i < weapons.Length; i++)
        {
            if (weaponsSO[i].isActived)
                weapons[i].SetActive(true);
        }
        //transform.position = GameObject.Find(player.positionLastCheckpoint).transform.Find("Spawn").position;
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }

    private void Start()
    {
        sphere1InitialPos = electroSphere1.transform.localPosition;
        sphere2InitialPos = electroSphere2.transform.localPosition;
        iceSpike.GetComponent<BoxCollider>().enabled = false;
        ActivateWeapons();
        player.hp = player.getGlobalMaxHP();

        Time.timeScale = 1;
    }


    public bool isMenuEnabled = false;

    public GameObject menuUI;
    public GameObject playerUI;


    public static Vector3 actualPosition;

    void Update()
    {
        actualPosition = playercenter.position;
        if (!isMenuEnabled)
            UpdateCamera();

        Fall();   
    }

    private void FixedUpdate()
    {
        if (!DoingAction)
            Move();
        else
            actualBlend = 0;

        if (direction != Vector3.zero)
            HandleRotation();

    }


    #region MOVEMENT

    private Vector3 direction;
    private float speed = 0f;
    [SerializeField]
    private float walkSpeed = 250f;
    [SerializeField]
    private float runSpeed = 350f;

    private bool isJumping = false;
    private bool isGrounded = true;
    private bool doubleJump = false;

    private bool isDashing = false;

    private float blend = 0f;
    private float actualBlend = 0f;

    public void Move()
    {
        float h = move.x;
        float v = move.y;

        direction = new Vector3(h, 0, v);
        direction = direction.normalized;

        if (!isDashing)
        {
            if (h == 0 && v == 0)
                blend = 0;
            else
            {
                if (running)
                {
                    speed = runSpeed;
                    blend = 1;
                }
                else
                {
                    speed = walkSpeed;
                    blend = 0.5f;
                }
            }
            actualBlend = Mathf.Lerp(anim.GetFloat("Blend"), blend, Time.deltaTime * 5f);
            anim.SetFloat("Blend", actualBlend);
            rb.velocity = new Vector3((Camera.main.transform.TransformDirection(direction) * speed * Time.deltaTime).x, rb.velocity.y, 
                (Camera.main.transform.TransformDirection(direction) * speed * Time.deltaTime).z);
        }
    }

    private void Run()
    {
        running = true;
    }

    private void StopRun()
    {
        running = false;
    }

    private void Dashing()
    {
        if (isGrounded && !isDashing && !isDoingSkill && !isMenuEnabled)
        {
            isDashing = true;
            canDoAttack = false;
            StartCoroutine(DashingIE());
        }
    }

    IEnumerator DashingIE()
    {
        rb.velocity = Vector3.zero;
        anim.SetTrigger("Dash");
        hurtbox.GetComponent<CapsuleCollider>().enabled = false;

        float h = move.x;
        float v = move.y;

        direction = new Vector3(h, 0, v);
        if (direction == Vector3.zero)
            rb.AddForce(new Vector3((transform.forward * 80000 * rb.mass * Time.fixedDeltaTime).x, rb.velocity.y, (transform.forward * 80000 * rb.mass * Time.fixedDeltaTime).z));
        else
        {
            direction = direction.normalized;
            direction = new Vector3(Camera.main.transform.TransformDirection(direction).x, rb.velocity.y, (Camera.main.transform.TransformDirection(direction).z));

            rb.AddForce(new Vector3((direction * 50000 * rb.mass * Time.fixedDeltaTime).x, rb.velocity.y, (direction * 50000 * rb.mass * Time.fixedDeltaTime).z));
        }
        yield return new WaitForSecondsRealtime(0.1f);
        canDoAttack = true;
        yield return new WaitForSecondsRealtime(0.2f);
        hurtbox.GetComponent<CapsuleCollider>().enabled = true;
        isDashing = false;
    }

    private void Jump()
    {
        if (isGrounded && !isMenuEnabled && !isJumping) 
        {
            isJumping = true;
            anim.SetTrigger("Jump");
            doubleJump = true;
            canDoAttack = false;
            StartCoroutine(CooldownJump());
            isGrounded = false;
        }
        else if (!isGrounded && !doubleJump && !isMenuEnabled) 
        {
            rb.velocity = new Vector2(0, 0);
            anim.SetTrigger("DoubleJump");
            rb.AddForce(new Vector2(0, 350f * 1.5f * rb.mass));
            doubleJump = true;
        }
    }

    IEnumerator CooldownJump()
    {
        yield return new WaitForSecondsRealtime(0.25f);
        rb.AddForce(new Vector2(0, 350f * 1.5f * rb.mass));
        yield return new WaitForSecondsRealtime(0.2f);
        doubleJump = false;
    }

    #endregion

    #region ATTACK
    private bool canDoAttack = true;

    private void Attack()
    {
        if (canDoAttack && isGrounded && !isMenuEnabled)
            StartCoroutine(AttackIE());
    }

    private IEnumerator AttackIE()
    {
        MoveDirectionWhileAttacking();
        DoingAction = true;
        rb.velocity = Vector3.zero;
        if (chain_attack_acc == 0)
        {
            chain_attack_acc = 1;
            StartCoroutine(Cooldown());
            anim.SetTrigger("Combo1");
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.swordslash);

            yield return new WaitForSecondsRealtime(0.5f);

            if (chain_attack_acc == 1)
            {
                chain_attack_acc = 0;
                DoingAction = false;
                ResetTriggers();
            }
        }
        else if (chain_attack_acc == 1)
        {
            chain_attack_acc = 2;
            StartCoroutine(Cooldown());
            anim.SetTrigger("Combo2");
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.swordslash);

            yield return new WaitForSecondsRealtime(0.5f);

            if (chain_attack_acc == 2)
            {
                chain_attack_acc = 0;
                DoingAction = false;
                ResetTriggers();
            }
        }
        else if (chain_attack_acc == 2 && player.skillsUnlocked.getUnlockedComboNodes() >= 1)
        {
            chain_attack_acc = 3;
            StartCoroutine(Cooldown());
            anim.SetTrigger("Combo3");
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.swordslash);

            yield return new WaitForSecondsRealtime(0.5f);

            if (chain_attack_acc == 3)
            {
                chain_attack_acc = 0;
                DoingAction = false;
                ResetTriggers();
            }
        }
        else if (chain_attack_acc == 3 && player.skillsUnlocked.getUnlockedComboNodes() >= 2)
        {
            chain_attack_acc = 4;
            StartCoroutine(Cooldown());
            anim.SetTrigger("Combo4");
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.swordslash);

            yield return new WaitForSecondsRealtime(0.5f);

            if (chain_attack_acc == 4)
            {
                chain_attack_acc = 0;
                DoingAction = false;
                ResetTriggers();
            }
        }
        else if (chain_attack_acc == 4 && player.skillsUnlocked.getUnlockedComboNodes() >= 3)
        {
            chain_attack_acc = 5;
            StartCoroutine(Cooldown());
            anim.SetTrigger("Combo5");
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.swordslash);

            yield return new WaitForSecondsRealtime(0.5f);

            if (chain_attack_acc == 5)
            {

                chain_attack_acc = 0;
                DoingAction = false;
                ResetTriggers();
            }
        }
        else if (chain_attack_acc == 5 && player.skillsUnlocked.getUnlockedComboNodes() >= 4)
        {
            chain_attack_acc = 6;
            StartCoroutine(Cooldown());
            anim.SetTrigger("Combo6");
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.swordslash);

            yield return new WaitForSecondsRealtime(1.28f);

            if (chain_attack_acc == 6)
            {
                chain_attack_acc = 0;
                DoingAction = false;
                ResetTriggers();
            }
        }
    }
    bool died = false;

    IEnumerator Die()
    {
        died = true;
        ResetTriggers();
        DoingAction = true;
        anim.SetTrigger("Die");
        hurtbox.GetComponent<CapsuleCollider>().enabled = false;

        yield return new WaitForSecondsRealtime(4f);

        anim.SetTrigger("Backtolife");
        died = false;
        hurtbox.GetComponent<CapsuleCollider>().enabled = true;
        DoingAction = false;
        canDoAttack = true;
        chain_attack_acc = 0;
        isHittedBulletRunning = false;
        transform.position = worldManager.getLastCheckpoint().position;
        rb.velocity = Vector3.zero;
    }

    private IEnumerator Cooldown()
    {
        canDoAttack = false;
        yield return new WaitForSecondsRealtime(0.15f);
        canDoAttack = true;
    }

    private void ResetTriggers()
    {
        anim.ResetTrigger("Combo1");
        anim.ResetTrigger("Combo2");
        anim.ResetTrigger("Combo3");
        anim.ResetTrigger("Combo4");
        anim.ResetTrigger("Combo5");
        anim.ResetTrigger("Combo6");
    }

    private void MoveDirectionWhileAttacking()
    {
        if (DoingAction)
        {
            float h = move.x;
            float v = move.y;

            direction = new Vector3(h, 0, v);

            if (direction != Vector3.zero) this.transform.forward = Vector3.Lerp(transform.forward, direction, Time.deltaTime * 50);
        }
    }

    public void HitEnemy(GameObject enemy, float damage, bool isBullet)
    {
        if (enemy.GetComponent<scr_BossPart>())
        {
            enemy.GetComponent<scr_BossPart>().hitted(damage);
        }
        else
        {
            enemy.GetComponentInParent<scr_EnemyControllerAdri>().Hitted(damage, isBullet);
        }
        //enemy.GetComponent<scr_EnemyDePrueba>().Hitted(damage);
    }

    #endregion

    #region SKILLS
    private bool isDoingSkill = false;

    public GameObject fireSkillUI;
    public GameObject electroSkillUI;
    public GameObject iceSkillUI;
    public GameObject windSkillUI;

    private float actualCooldownFire;
    private float actualCooldownElectro;
    private float actualCooldownIce;
    private float actualCooldownWind;

    public GameObject fireCircle;
    private Vector3 sphere1InitialPos;
    private Vector3 sphere2InitialPos;
    public GameObject electroSphere1;
    public GameObject electroSphere2;
    public GameObject iceSpike;
    public GameObject windTornado;

    public GameObject fireCirclelvl2;
    public GameObject electrolvl2;
    public GameObject iceSpikelvl2;
    public GameObject windTornadolvl2;

    private void FireSkill()
    {
        if (isGrounded && canDoAttack && !DoingAction && !isDoingSkill && !isMenuEnabled &&
            player.skillsUnlocked.getUnlockedFireNodes() > 0 && actualCooldownFire == 0)
                StartCoroutine(FireSkillIE());
    }

    private void IceSkill()
    {
        if (isGrounded && canDoAttack && !DoingAction && !isDoingSkill && !isMenuEnabled &&
            player.skillsUnlocked.getUnlockedIceNodes() > 0 && actualCooldownIce == 0)
                StartCoroutine(IceSkillIE());
    }

    private void WindSkill()
    {
        if (isGrounded && canDoAttack && !DoingAction && !isDoingSkill && !isMenuEnabled &&
            player.skillsUnlocked.getUnlockedWindNodes() > 0 && actualCooldownWind == 0)
                StartCoroutine(WindSkillIE());
    }

    private void ElectroSkill()
    {
        if (isGrounded && canDoAttack && !DoingAction && !isDoingSkill && !isMenuEnabled &&
            player.skillsUnlocked.getUnlockedElectNodes() > 0 && actualCooldownElectro == 0)
                StartCoroutine(ElectroSkillIE());

        

    }
    IEnumerator FireSkillIE()
    {
        anim.SetTrigger("FireSkill");
        DeactivateInputBeforeSkill();
        StartCoroutine(UpdateSkillUI(fireSkillUI, so_NodeSkills.stat.FIRE, player.skillsUnlocked.cooldownFireSkill));
        yield return new WaitForSecondsRealtime(2.65f);
        ActivateInputAfterSkill();
    }

    IEnumerator IceSkillIE()
    {
        anim.SetTrigger("IceSkill");
        DeactivateInputBeforeSkill();
        StartCoroutine(UpdateSkillUI(iceSkillUI, so_NodeSkills.stat.ICE, player.skillsUnlocked.cooldownIceSkill));
        yield return new WaitForSecondsRealtime(3.24f);
        ActivateInputAfterSkill();
    }

    IEnumerator WindSkillIE()
    {
        anim.SetTrigger("WindSkill");
        DeactivateInputBeforeSkill();
        StartCoroutine(UpdateSkillUI(windSkillUI, so_NodeSkills.stat.WIND, player.skillsUnlocked.cooldownWindSkill));
        yield return new WaitForSecondsRealtime(1.8f);
        ActivateInputAfterSkill();
    }

    IEnumerator ElectroSkillIE()
    {
        anim.SetTrigger("ElectroSkill");
        DeactivateInputBeforeSkill();
        StartCoroutine(UpdateSkillUI(electroSkillUI, so_NodeSkills.stat.ELEC, player.skillsUnlocked.cooldownElectSkill));
        yield return new WaitForSecondsRealtime(1.04f);
        ActivateInputAfterSkill();
    }

    void ElectroLevel1()
    {
        StartCoroutine(MoveSphere()); 
    }

    void IceLevel1()
    {
        StartCoroutine(SpawnIce());
    }

    void FireLevel1()
    {
        StartCoroutine(SpawnFire());
    }

    void WindLevel1()
    {
        StartCoroutine(SpawnTornado());
    }

    IEnumerator SpawnIce()
    {
        scr_AudioManager.instance.playSound(scr_AudioManager.sound.iceskill);
        GameObject auxice;
        if (player.skillsUnlocked.getUnlockedIceNodes() > 1)
            auxice = iceSpikelvl2;
        else
            auxice = iceSpike;

        auxice.SetActive(true);
        auxice.GetComponent<VisualEffect>().Play();
        Vector3 initalPos = auxice.transform.localPosition;
        auxice.transform.position = new Vector3(100, 100, 100);
        auxice.GetComponent<BoxCollider>().enabled = true;
        yield return new WaitForEndOfFrame();
        auxice.transform.localPosition = initalPos;
        yield return new WaitForEndOfFrame();
        auxice.transform.SetParent(null);
        yield return new WaitForSecondsRealtime(1f);
        auxice.GetComponent<BoxCollider>().enabled = false;
        yield return new WaitForSecondsRealtime(2f);
        auxice.transform.SetParent(this.transform.GetChild(3));
        auxice.transform.localPosition = initalPos;
        auxice.SetActive(false);
        scr_AudioManager.instance.stopSound(scr_AudioManager.sound.iceskill);
    }

    IEnumerator SpawnFire()
    {
        scr_AudioManager.instance.playSound(scr_AudioManager.sound.flameskill);
        bool auxlvl2 = false;
        Vector3 initialPos2 = Vector3.zero;
        if (player.skillsUnlocked.getUnlockedFireNodes() > 1)
            auxlvl2 = true;

        fireCircle.SetActive(true);
        fireCircle.GetComponent<VisualEffect>().Reinit();
        Vector3 initalPos = fireCircle.transform.localPosition;
        fireCircle.transform.position = new Vector3(100, 100, 100);

        if (auxlvl2)
        {
            fireCircle.GetComponent<SphereCollider>().enabled = false;
            fireCirclelvl2.SetActive(true);
            fireCirclelvl2.GetComponent<VisualEffect>().Reinit();
            initialPos2 = fireCirclelvl2.transform.localPosition;
            fireCirclelvl2.transform.position = new Vector3(100, 100, 100);
        }

        yield return new WaitForEndOfFrame();
        fireCircle.transform.localPosition = initalPos;
        if (auxlvl2)
            fireCirclelvl2.transform.localPosition = initialPos2;
        yield return new WaitForEndOfFrame();
        fireCircle.GetComponent<VisualEffect>().playRate = 2.5f;
        fireCircle.transform.SetParent(null);
        if (auxlvl2)
        {
            fireCirclelvl2.GetComponent<VisualEffect>().playRate = 2.5f;
            fireCirclelvl2.transform.SetParent(null);
        }
        yield return new WaitForSecondsRealtime(1f);
        fireCircle.GetComponent<VisualEffect>().Stop();
        if (auxlvl2)
            fireCirclelvl2.GetComponent<VisualEffect>().Stop();
        yield return new WaitForSecondsRealtime(1.5f);
        if (auxlvl2)
        {
            fireCirclelvl2.transform.SetParent(this.transform.GetChild(3));
            fireCirclelvl2.transform.localPosition = initialPos2;
            fireCirclelvl2.SetActive(false);
            fireCircle.GetComponent<SphereCollider>().enabled = true;
        }
        fireCircle.transform.SetParent(this.transform.GetChild(3));
        fireCircle.transform.localPosition = initalPos;
        fireCircle.SetActive(false);
        scr_AudioManager.instance.stopSound(scr_AudioManager.sound.flameskill);

    }

    IEnumerator MoveSphere()
    {
        if (player.skillsUnlocked.getUnlockedElectNodes() > 1)
        {
            StartCoroutine(SpawnLightning());
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.thunderskill2);
        }
        float time = 0;
        electroSphere1.gameObject.SetActive(true);
        electroSphere2.gameObject.SetActive(true);
        electroSphere1.transform.localPosition = sphere1InitialPos;
        electroSphere2.transform.localPosition = sphere2InitialPos;

        scr_AudioManager.instance.playSound(scr_AudioManager.sound.thunderskill);
        while (time < 4.5)
        {
            electroSphere1.transform.RotateAround(this.transform.position, -Vector3.up, 500f * Time.deltaTime);
            electroSphere2.transform.RotateAround(this.transform.position, -Vector3.up, 500f * Time.deltaTime);
            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        electroSphere1.transform.GetComponentInChildren<VisualEffect>().Stop();
        electroSphere2.transform.GetComponentInChildren<VisualEffect>().Stop();
        time = 0;

        while (time < 2)
        {
            electroSphere1.transform.RotateAround(this.transform.position, -Vector3.up, 500f * Time.deltaTime);
            electroSphere2.transform.RotateAround(this.transform.position, -Vector3.up, 500f * Time.deltaTime);
            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        electroSphere1.gameObject.SetActive(false);
        electroSphere2.gameObject.SetActive(false);
        scr_AudioManager.instance.stopSound(scr_AudioManager.sound.thunderskill);
    }

    IEnumerator SpawnLightning()
    {
        electrolvl2.SetActive(true);
        Vector3 initalPos = electrolvl2.transform.localPosition;
        electrolvl2.transform.position = new Vector3(100, 100, 100);
        electrolvl2.GetComponent<BoxCollider>().enabled = true;
        yield return new WaitForEndOfFrame();
        electrolvl2.transform.localPosition = initalPos;
        yield return new WaitForEndOfFrame();
        electrolvl2.transform.SetParent(null);
        yield return new WaitForSecondsRealtime(1f);
        electrolvl2.GetComponent<BoxCollider>().enabled = false;
        yield return new WaitForSecondsRealtime(2f);
        electrolvl2.transform.SetParent(this.transform.GetChild(3));
        electrolvl2.transform.localPosition = initalPos;
        electrolvl2.SetActive(false);
        scr_AudioManager.instance.stopSound(scr_AudioManager.sound.thunderskill2);
    }

    IEnumerator SpawnTornado()
    {
        GameObject auxwind;
        if (player.skillsUnlocked.getUnlockedWindNodes() > 1)
            auxwind = windTornadolvl2;
        else
            auxwind = windTornado;
        scr_AudioManager.instance.playSound(scr_AudioManager.sound.tornado);

        auxwind.SetActive(true);
        Vector3 initalPos = auxwind.transform.localPosition;
        Quaternion initalRotate = auxwind.transform.localRotation;
        auxwind.GetComponent<BoxCollider>().enabled = true;
        yield return new WaitForEndOfFrame();
        auxwind.transform.localPosition = initalPos;
        yield return new WaitForEndOfFrame();
        auxwind.transform.SetParent(null);

        float time = 0;
        while (time < 4)
        {
            auxwind.GetComponent<Rigidbody>().velocity = auxwind.transform.forward * 200f * Time.fixedDeltaTime;
            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        auxwind.GetComponent<BoxCollider>().enabled = false;
        auxwind.transform.SetParent(this.transform.GetChild(3));
        auxwind.transform.localPosition = initalPos;
        auxwind.transform.localRotation = initalRotate;
        auxwind.SetActive(false);
        scr_AudioManager.instance.stopSound(scr_AudioManager.sound.tornado);
    }


    private void DeactivateInputBeforeSkill()
    {
        isDoingSkill = true;
        DoingAction = true;
        canDoAttack = false;
        rb.velocity = Vector3.zero;
        hurtbox.GetComponent<CapsuleCollider>().enabled = false;
    }


    private void ActivateInputAfterSkill()
    {
        if (!died)
        {
            DoingAction = false;
            canDoAttack = true;
            hurtbox.GetComponent<CapsuleCollider>().enabled = true;
        }
        isDoingSkill = false;
    }

    IEnumerator UpdateSkillUI(GameObject skillToUpdate, so_NodeSkills.stat type, float maxTimerSkill)
    {
        Color colorSkill = skillToUpdate.GetComponent<Image>().color;
        skillToUpdate.GetComponent<Image>().color = new Color(0, 0, 0, 0.2f);
        TextMeshProUGUI text = skillToUpdate.transform.GetChild(2).gameObject.GetComponent<TextMeshProUGUI>();
        text.gameObject.SetActive(true);
        Image iconElement = skillToUpdate.transform.GetChild(1).gameObject.GetComponent<Image>();
        iconElement.color = new Color(1f, 1f, 1f, 0.1f);

        Image image = skillToUpdate.transform.GetChild(0).gameObject.GetComponent<Image>();
        image.color = new Color(1, 1, 1, 0.1f);
        image.fillAmount = 0;

        if (type == so_NodeSkills.stat.WIND)
        {
            actualCooldownWind = maxTimerSkill;
            while (actualCooldownWind > 0)
            {
                actualCooldownWind -= Time.deltaTime;
                image.fillAmount = 1 - (actualCooldownWind / maxTimerSkill);
                text.text = string.Format("{0:0.0}", actualCooldownWind);
                yield return new WaitForEndOfFrame();
            }
            actualCooldownWind = 0;
        }
        else if (type == so_NodeSkills.stat.ELEC)
        {
            actualCooldownElectro = maxTimerSkill;
            while (actualCooldownElectro > 0)
            {
                actualCooldownElectro -= Time.deltaTime;
                image.fillAmount = 1 - (actualCooldownElectro / maxTimerSkill);
                text.text = string.Format("{0:0.0}", actualCooldownElectro);
                yield return new WaitForEndOfFrame();
            }
            actualCooldownElectro = 0;
        }
        else if (type == so_NodeSkills.stat.FIRE)
        {
            actualCooldownFire = maxTimerSkill;
            while (actualCooldownFire > 0)
            {
                actualCooldownFire -= Time.deltaTime;
                image.fillAmount = 1 - (actualCooldownFire / maxTimerSkill);
                text.text = string.Format("{0:0.0}", actualCooldownFire);
                yield return new WaitForEndOfFrame();
            }
            actualCooldownFire = 0;
        }
        else if (type == so_NodeSkills.stat.ICE)
        {
            actualCooldownIce = maxTimerSkill;
            while (actualCooldownIce > 0)
            {
                actualCooldownIce -= Time.deltaTime;
                image.fillAmount = 1 - (actualCooldownIce / maxTimerSkill);
                text.text = string.Format("{0:0.0}", actualCooldownIce);
                yield return new WaitForEndOfFrame();
            }
            actualCooldownIce = 0;
        }
        text.gameObject.SetActive(false);
        skillToUpdate.GetComponent<Image>().color = colorSkill;
        iconElement.color = new Color(0.87f, 0.87f, 0.87f, 0.8f);
    }

    #endregion

    #region CAMERA

    public float smoothRotationCamera = 300f;
    private float zoom;
    Vector3 lastPosition;

    private void UpdateCamera()
    {
        if (mainCamera.enabled)
        {
            mainCamera.transform.position += this.transform.position - lastPosition;
            lastPosition = this.transform.position;
            mainCamera.transform.LookAt(new Vector3(this.transform.position.x, this.transform.position.y + 1, this.transform.position.z));
            mainCamera.transform.RotateAround(this.transform.position, -Vector3.up, -rotate.x * smoothRotationCamera * Time.smoothDeltaTime);
            HideAndLockCursor(true);
            MouseWheeling();
        }
    }

    private void HideAndLockCursor(bool locked)
    {
        if (locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    private void MouseWheeling()
    {
        Vector3 pos = mainCamera.transform.position;
        if (zoom < 0)
        {
            if (Vector3.Distance(mainCamera.transform.position, this.transform.position) < 10)
            {
                pos = pos - mainCamera.transform.forward * Time.deltaTime * 50;
                //Vector3.Lerp(pos, pos - mainCamera.transform.forward, 1f);
                mainCamera.transform.position = pos;
            }
        }
        if (zoom > 0)
        {
            if (Vector3.Distance(mainCamera.transform.position, this.transform.position) > 3.5f)
            {
                pos = pos + mainCamera.transform.forward * Time.deltaTime * 50;
                mainCamera.transform.position = pos;
            }
        }
    }

    private void HandleRotation()
    {
        float targetRotation = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;
        Quaternion lookAt = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, targetRotation, 0), 0.5f);
        rb.rotation = lookAt;
    }

    private void ZoomIn()
    {
        zoom = 1f;
    }

    private void ZoomOut()
    {
        zoom = -1f;
    }

    private void ZoomCancel()
    {
        zoom = 0;
    }
    #endregion

    #region MENU
    public void OpenMainMenu()
    {
        if (!isMenuEnabled)
        {
            Time.timeScale = 0f;
            isMenuEnabled = !isMenuEnabled;
            menuUI.GetComponentInChildren<scr_MenuManager>().BackToTheMenu();
            menuUI.SetActive(true);
            playerUI.SetActive(false);
            HideAndLockCursor(false);
            transform.GetComponentInChildren<scr_Volume>().InMenu(false);
            //mainCamera.GetComponent<Camera>().enabled = false;
        }
        else
        {
            Time.timeScale = 1f;
            isMenuEnabled = !isMenuEnabled;
            menuUI.SetActive(false);
            playerUI.SetActive(true);
            hpBarPlayer.RestartHP();
            HideAndLockCursor(true);
            //mainCamera.GetComponent<Camera>().enabled = true;
        }
    }
    #endregion

    private void ActivateWeapons()
    {
        int index = 0;
        foreach (so_Weapon w in weaponsSO)
        {
            if (w.isActived)
                weapons[index].SetActive(true);
            else
                weapons[index].SetActive(false);
            index++;
        }
    }
    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Ground")
        {
            if (!isGrounded)
            {
                anim.SetTrigger("Grounded");
                doubleJump = false;
                canDoAttack = true;
            }
            isGrounded = true;
            isJumping = false;
            anim.ResetTrigger("DoubleJump");
            anim.ResetTrigger("Jump");
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            if (!isGrounded)
            {
                anim.SetTrigger("Grounded");
                doubleJump = false;
                canDoAttack = true;
            }
            isGrounded = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            isGrounded = false;
            anim.ResetTrigger("Grounded");
        }
    }

    public scr_PlayerBar hpBarPlayer;

    IEnumerator Hitted(bool isBoss)
    {
        print("pupita");
        lerpTimerHPbar = 0;
        rb.velocity = Vector3.zero;
        DoingAction = true;
        anim.SetTrigger("TakeDamage");
        anim.SetFloat("Blend", 0f);
        if(!isBoss)
            player.hp -= 10;
        else
            player.hp -= 50;
        hpBarPlayer.UpdateHealthUI();
        canDoAttack = false;
        if(player.hp <= 0)
            StartCoroutine(Die());
        else
        {
            
            yield return new WaitForSecondsRealtime(0.65f);
            if (!died)
            {
                DoingAction = false;
                canDoAttack = true;
            }
        }

        chain_attack_acc = 0;
        isHittedBulletRunning = false;
    }

    private void Fall()
    {
        if(transform.position.y < -10)
        {
            transform.position = worldManager.getLastCheckpoint().position;
            rb.velocity = Vector3.zero;
        }
    }


    public bool isHittedBulletRunning = false;

    public void HittedBullet(bool isBoss)
    {
        print("bobo");
        if (!isHittedBulletRunning && hurtbox.GetComponent<CapsuleCollider>().enabled == true)
        {
            isHittedBulletRunning = true;
            StartCoroutine(Hitted(isBoss));
        }
            
    }
}
