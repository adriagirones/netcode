﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_BossPart : MonoBehaviour
{
    private scr_Boss boss;

    public enum Part
    {
        ARM,
        CANON,
        COCKPIT,
        OVERHEAT
    }

    public Part part;

    // Start is called before the first frame update
    void Start()
    {
        boss = transform.GetComponentInParent<scr_Boss>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void hitted(float damage)
    {
        boss.Hitted(damage, part);
    }
}
