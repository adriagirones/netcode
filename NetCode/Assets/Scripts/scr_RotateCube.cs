﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_RotateCube : MonoBehaviour
{
    public float velocity;
    
    void Update()
    {
        switch(Random.Range(0, 3)){
            case 0:
                transform.RotateAround(transform.position, Vector3.right, velocity * Time.deltaTime);
                break;
            case 1:
                transform.RotateAround(transform.position, Vector3.forward, velocity * Time.deltaTime);
                break;
            case 2:
                transform.RotateAround(transform.position, Vector3.up, velocity * Time.deltaTime);
                break;

        }
        
       
        
    }
}
