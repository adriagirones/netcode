﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_EnemyPodGunner : scr_EnemyControllerAdri
{
    public Transform position0Turret;
    public Transform position1Turret;
    private int lastPosition = 0;
    private bool reachedFinalMovement = true;
    private Vector3 toGoPosition;
    public float durationAttack = 2f;
    public scr_DaniBulletManager bulletManager;
    public Transform positionGun;

    protected override void OnStart_Deactivated()
    {
        changeState(EnemyStates.RETURN_INIT_POS);
    }

    protected override void Update_ReturnInitPos()
    {
        checkFinalMovement();
        if (lastPosition == 0 && reachedFinalMovement)
        {
            toGoPosition = position0Turret.position;
            agent.SetDestination(toGoPosition);
            reachedFinalMovement = false;
            lastPosition = 1;
        }
        else if (lastPosition == 1 && reachedFinalMovement)
        {
            toGoPosition = position1Turret.position;
            agent.SetDestination(toGoPosition);
            reachedFinalMovement = false;
            lastPosition = 0;
        }
    }

    private void checkFinalMovement()
    {
        if (lastPosition == 0 && Mathf.Approximately(position1Turret.position.x, transform.position.x) &&
            Mathf.Approximately(position1Turret.position.z, transform.position.z))
            reachedFinalMovement = true;
        else if (lastPosition == 1 && Mathf.Approximately(position0Turret.position.x, transform.position.x) &&
            Mathf.Approximately(position0Turret.position.z, transform.position.z))
            reachedFinalMovement = true;
    }


    #region ATTACK
    public float cooldownAttack = 3f;
    bool rafagaSoltada = false;
    protected override void OnStart_Attacking()
    {
        agent.isStopped = true;
        rafagaSoltada = true;
        anim.ResetTrigger("attackIdle");
        StartCoroutine(AttackingTime());
    }
    protected override void Update_Attacking()
    {
        LookAtButMoreSmooth();
        if (!rafagaSoltada)
        {
            StartCoroutine(AttackingTime());
            rafagaSoltada = true;
        }
    }

    void LookAtButMoreSmooth()
    {
        if (target != null)
        {
            Vector3 relativePos = target.transform.position - transform.position;
            Quaternion toRotation = Quaternion.LookRotation(relativePos);
            transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, 5f * Time.deltaTime);
        }
    }

    protected override void OnDisable_Attacking()
    {
        agent.isStopped = false;
        StopAllCoroutines();
        anim.ResetTrigger("attack");
        anim.ResetTrigger("attackIdle");
    }

    IEnumerator AttackingTime()
    {
        
        yield return new WaitForSecondsRealtime(cooldownHitted);
        anim.SetTrigger("attack");
        float timer = 0;
        while (timer < durationAttack)
        {
            bulletManager.GetComponent<scr_DaniBulletManager>().startShooting(enemyInfo, positionGun);
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.shoot2);
            yield return new WaitForSecondsRealtime(durationAttack / enemyInfo.cadency);
            timer += (durationAttack / enemyInfo.cadency);
        }
        anim.SetTrigger("attackIdle");
        yield return new WaitForSecondsRealtime(cooldownAttack);

        rafagaSoltada = false;
    }

    #endregion


    public override void EnterAreaAction()
    {
        changeState(EnemyStates.ATTACKING);
    }

    public override void ExitAreaAction()
    {
        StopAllCoroutines();
        changeState(EnemyStates.RETURN_INIT_POS);
    }

    #region HITTED
    public float iFrames = 0.2f;
    private float iFramesTimer = 0f;
    public float cooldownHitted = 3f;

    public override void Hitted(float damage, bool isBullet)
    {
        if (!hpbar.activeSelf)
            hpbar.SetActive(true);
        if (iFramesTimer == 0)
        {
            hp -= damage;
            lerpTimer = 0;
            if (hp <= 0)
            {
                changeState(EnemyStates.DYING);
                hp = 0;
            }
            else if(!isBullet)
            {
                changeState(EnemyStates.HITTED);
            }
        }
    }

    protected override void OnStart_Hitted()
    {
        StopAllCoroutines();
        anim.SetTrigger("hitted");
        agent.isStopped = true;
    }

    protected override void Update_Hitted()
    {
        LookAtButMoreSmooth();
        iFramesTimer += Time.deltaTime;
        if (iFramesTimer > iFrames)
        {
          //  cooldownHittedTimer += Time.deltaTime;
          //  if(cooldownHittedTimer >= cooldownHitted)
           // {
                changeState(EnemyStates.ATTACKING);
          //  }
        }
            
    }

    protected override void OnDisable_Hitted()
    {
        iFramesTimer = 0;
        agent.isStopped = false;
    }
    #endregion

    #region DYING
    protected override void OnStart_Dying()
    {
        anim.ResetTrigger("hitted");
        anim.ResetTrigger("running");
        anim.ResetTrigger("attack");
        anim.ResetTrigger("attackIdle");
        anim.SetTrigger("die");
        StartCoroutine(Die());
        transform.Find("EnemyHurtbox").GetComponent<CapsuleCollider>().enabled = false;
        agent.isStopped = true;
        transform.Find("PodCenter").transform.position = new Vector3(5000, 5000, 5000);
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(4f);
        scr_PlayerController.instance.player.skillPoints += 4;
        scr_PlayerController.instance.player.upExp(10);
        GameObject.Destroy(this.gameObject);
    }
    #endregion
}
