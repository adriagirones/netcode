﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_EnemyPodMelee : scr_EnemyControllerAdri
{
    public Transform[] routepoints;
    public float stopFollowingTime = 2f;
    public float distanceToAttack = 2.2f;

    #region DEACTIVATED
    private int actualPoint;
    private float timerPatrol;
    protected override void OnStart_Deactivated()
    {
        anim.SetTrigger("poweron");
        changeState(EnemyStates.RETURN_INIT_POS);
    }
    #endregion

    #region RETURN_INIT_POS
    protected override void OnStart_ReturnInitPos()
    {
        //actualPoint = 0;
        timerPatrol = 0f;
    }
    protected override void Update_ReturnInitPos()
    {
        if (timerPatrol == 0)
        {
            agent.SetDestination(routepoints[actualPoint].position);
            if (Mathf.Approximately(routepoints[actualPoint].position.x, transform.position.x) && Mathf.Approximately(routepoints[actualPoint].position.z, transform.position.z))
            {
                actualPoint++;
                if (actualPoint == routepoints.Length) actualPoint = 0;
                timerPatrol = 3f;
                anim.SetTrigger("poweroff");
            }
        }
        else
        {
            timerPatrol -= Time.deltaTime;
            if (timerPatrol < 0)
            {
                timerPatrol = 0;
                anim.SetTrigger("poweron");
            }
        }
    }
    protected override void OnDisable_ReturnInitPos()
    {
        anim.ResetTrigger("poweron");
        anim.ResetTrigger("poweroff");
    }
    #endregion

    #region FOLLOW_PLAYER
    protected override void OnStart_FollowPlayer()
    {
        AnimatorClipInfo[] currentAnim = anim.GetCurrentAnimatorClipInfo(0);
        if (currentAnim.Length > 0 && currentAnim[0].clip.name == "Robo3 Power off")
            anim.SetTrigger("poweron");
        else
            anim.SetTrigger("running");

    }
    protected override void Update_FollowPlayer()
    {
        agent.SetDestination(target.position);
        //print(Vector3.Distance(target.transform.position, this.transform.GetChild(1).transform.position));

        if (Vector3.Distance(target.transform.position, this.transform.GetChild(1).transform.position) <= distanceToAttack)
            changeState(EnemyStates.ATTACKING);
    }
    protected override void OnDisable_FollowPlayer()
    {
        anim.ResetTrigger("poweron");
    }
    #endregion

    #region FOLLOW_BUT_NOT_SEEING
    private float actualStopFollowingTime = 0f;
    protected override void OnStart_FollowButNotSeeing()
    {
        actualStopFollowingTime = 0f;

        anim.SetTrigger("running");

    }
    protected override void Update_FollowButNotSeeing()
    {
        agent.SetDestination(target.position);
        actualStopFollowingTime += Time.deltaTime;
        if (actualStopFollowingTime >= stopFollowingTime)
        {
            changeState(EnemyStates.RETURN_INIT_POS);
        }
    }
    protected override void OnDisable_FollowButNotSeeing()
    {
        actualStopFollowingTime = 0f;
    }
    #endregion

    #region ATTACK
    private float cooldownAttack = 0f;
    public float durationAttack = 2.5f;
    protected override void OnStart_Attacking()
    {
        agent.isStopped = true;
        cooldownAttack = 0;
        if(Random.Range(0,2) == 0)
            anim.SetTrigger("attack");
        else
            anim.SetTrigger("attack2");
    }
    protected override void Update_Attacking()
    {
        transform.LookAt(new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z));
        cooldownAttack += Time.deltaTime;
        if (cooldownAttack >= durationAttack)
        {
            changeState(EnemyStates.FOLLOW_PLAYER);
        }
            
    }
    protected override void OnDisable_Attacking()
    {
        agent.isStopped = false;
        cooldownAttack = 0;
        anim.ResetTrigger("attack");
        anim.ResetTrigger("attack2");
        
    }
    #endregion

    public override void EnterAreaAction()
    {
        changeState(EnemyStates.FOLLOW_PLAYER);
    }

    public override void ExitAreaAction()
    {
        changeState(EnemyStates.FOLLOW_BUT_NOT_SEEING);
    }

    #region HITTED
    public float iFrames = 0.2f;
    private float iFramesTimer = 0f;
    public override void Hitted(float damage, bool isBullet)
    {
        if (!hpbar.activeSelf)
            hpbar.SetActive(true);
        if (iFramesTimer == 0)
        {
            hp -= damage;
            lerpTimer = 0;
            if (hp <= 0)
            {
                changeState(EnemyStates.DYING);
                hp = 0;
            }
            else if (!isBullet)
                changeState(EnemyStates.HITTED);
        }
    }
    protected override void OnStart_Hitted()
    {
        anim.SetTrigger("hitted");
        agent.isStopped = true;
    }

    protected override void Update_Hitted()
    {

        transform.LookAt(new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z));
        iFramesTimer += Time.deltaTime;
        if (iFramesTimer > iFrames)
            changeState(EnemyStates.FOLLOW_PLAYER);
    }

    protected override void OnDisable_Hitted()
    {
        iFramesTimer = 0;
        agent.isStopped = false;
    }
    #endregion

    #region DYING
    protected override void OnStart_Dying()
    {
        anim.ResetTrigger("hitted");
        anim.ResetTrigger("running");
        anim.ResetTrigger("attack");
        anim.ResetTrigger("attack2");
        anim.ResetTrigger("poweron");
        anim.ResetTrigger("poweroff");
        anim.SetTrigger("die");
        StartCoroutine(Die());
        transform.Find("EnemyHurtbox").GetComponent<CapsuleCollider>().enabled = false;
        agent.isStopped = true;
        transform.Find("PodCenter").transform.position = new Vector3(5000, 5000, 5000);
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(4f);
        scr_PlayerController.instance.player.skillPoints += 4;
        scr_PlayerController.instance.player.upExp(10);
        GameObject.Destroy(this.gameObject);
    }
    #endregion
}
