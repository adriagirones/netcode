﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class scr_EnemyBar : MonoBehaviour
{
    public float chipSpeed = 2f;
    public Transform frontHealthBar;
    public Transform backHealthBar;
    public scr_EnemyControllerAdri enemy;


    // Update is called once per frame
    void Update()
    {
        UpdateHealthUI();
    }

    void UpdateHealthUI()
    {
        this.transform.forward = Camera.main.transform.forward;
        float fillF = frontHealthBar.localScale.x;
        float fillB = backHealthBar.localScale.x;
        float hFraction = enemy.hp / enemy.enemyInfo.maxHp;
        if(fillB > hFraction)
        {
            frontHealthBar.localScale = new Vector2(hFraction,1);
            enemy.lerpTimer += Time.deltaTime;
            float percentComplete = enemy.lerpTimer / chipSpeed;
            backHealthBar.localScale= new Vector2(Mathf.Lerp(fillB, hFraction, percentComplete),1);
        }
        if(fillF < hFraction)
        {
            backHealthBar.localScale = new Vector2(hFraction,1);
            enemy.lerpTimer += Time.deltaTime;
            float percentComplete = enemy.lerpTimer / chipSpeed;
            percentComplete *= percentComplete;
            frontHealthBar.localScale = new Vector2(Mathf.Lerp(fillF, backHealthBar.localScale.x, percentComplete),1);
        }
    }
}
