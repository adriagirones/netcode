﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class scr_EnemyControllerAdri : MonoBehaviour
{
    public enum EnemyStates
    {
        DEACTIVATED,
        FOLLOW_PLAYER,
        ATTACKING,
        FOLLOW_BUT_NOT_SEEING,
        RETURN_INIT_POS,
        HITTED,
        DYING
    }

    protected NavMeshAgent agent;
    public Animator anim;

    public so_Enemies enemyInfo;
    public EnemyStates actualState;

    protected Transform target;
    protected Vector3 initPosition;
    protected Quaternion initRotation;

    public float hp;
    [HideInInspector]
    public float lerpTimer = 0f;
    public GameObject hpbar;

    bool IsInWorldRed;
    float normalSpeed;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        hp = enemyInfo.maxHp;
        initPosition = this.transform.position;
        initRotation = this.transform.rotation;
    }

    private void Start()
    {
        hpbar.SetActive(false);
        actualState = EnemyStates.DEACTIVATED;
        OnStart_Deactivated();
        normalSpeed = agent.speed;
    }

    private void Update()
    {
        switch (actualState)
        {
            case EnemyStates.DEACTIVATED:
                Update_Deactivated();
                break;
            case EnemyStates.FOLLOW_PLAYER:
                Update_FollowPlayer();
                break;
            case EnemyStates.FOLLOW_BUT_NOT_SEEING:
                Update_FollowButNotSeeing();
                break;
            case EnemyStates.ATTACKING:
                Update_Attacking();
                break;
            case EnemyStates.RETURN_INIT_POS:
                Update_ReturnInitPos();
                break;
            case EnemyStates.HITTED:
                Update_Hitted();
                break;
            case EnemyStates.DYING:
                Update_Dying();
                break;
        }
    }

    public void changeState(EnemyStates newState)
    {
        switch (actualState)
        {
            case EnemyStates.DEACTIVATED:
                OnDisable_Deactivated();
                break;
            case EnemyStates.FOLLOW_PLAYER:
                OnDisable_FollowPlayer();
                break;
            case EnemyStates.FOLLOW_BUT_NOT_SEEING:
                OnDisable_FollowButNotSeeing();
                break;
            case EnemyStates.ATTACKING:
                OnDisable_Attacking();
                break;
            case EnemyStates.RETURN_INIT_POS:
                OnDisable_ReturnInitPos();
                break;
            case EnemyStates.HITTED:
                OnDisable_Hitted();
                break;
            case EnemyStates.DYING:
                OnDisable_Dying();
                break;
        }
        switch (newState)
        {
            case EnemyStates.DEACTIVATED:
                OnStart_Deactivated();
                break;
            case EnemyStates.FOLLOW_PLAYER:
                scr_AudioManager.instance.playSound(scr_AudioManager.sound.enemyFollowandBossState);
                OnStart_FollowPlayer();
                break;
            case EnemyStates.FOLLOW_BUT_NOT_SEEING:
                OnStart_FollowButNotSeeing();
                break;
            case EnemyStates.ATTACKING:
                OnStart_Attacking();
                break;
            case EnemyStates.RETURN_INIT_POS:
                scr_AudioManager.instance.playSound(scr_AudioManager.sound.enemyFollowandBossState);
                OnStart_ReturnInitPos();
                break;
            case EnemyStates.HITTED:
                scr_AudioManager.instance.playSound(scr_AudioManager.sound.robothit);
                OnStart_Hitted();
                break;
            case EnemyStates.DYING:
                scr_AudioManager.instance.playSound(scr_AudioManager.sound.robotsound);
                OnStart_Dying();
                break;
        }
        actualState = newState;
    }

    protected virtual void OnStart_Deactivated() { }
    protected virtual void Update_Deactivated() { }
    protected virtual void OnDisable_Deactivated() { }
    protected virtual void OnStart_FollowPlayer() { }
    protected virtual void Update_FollowPlayer() { }
    protected virtual void OnDisable_FollowPlayer() { }
    protected virtual void OnStart_FollowButNotSeeing() { }
    protected virtual void Update_FollowButNotSeeing() { }
    protected virtual void OnDisable_FollowButNotSeeing() { }  
    protected virtual void OnStart_Attacking() { }
    protected virtual void Update_Attacking() { }
    protected virtual void OnDisable_Attacking() { }
    protected virtual void OnStart_ReturnInitPos() { }
    protected virtual void Update_ReturnInitPos() { }
    protected virtual void OnDisable_ReturnInitPos() { }
    protected virtual void OnStart_Hitted() { }
    protected virtual void Update_Hitted() { }
    protected virtual void OnDisable_Hitted() { }
    protected virtual void OnStart_Dying() { }
    protected virtual void Update_Dying() { }
    protected virtual void OnDisable_Dying() { }

    public abstract void EnterAreaAction();
    public abstract void ExitAreaAction();

    public abstract void Hitted(float damage, bool isBullet);

    public void setTarget(Transform playerTarget)
    {
        if (target == null)
            target = playerTarget;
    }

    public void OnChangeWorld()
    {
        IsInWorldRed = !IsInWorldRed;
        if (IsInWorldRed)
            agent.speed = normalSpeed / 3;
        else
            agent.speed = normalSpeed;
 
    }

}
