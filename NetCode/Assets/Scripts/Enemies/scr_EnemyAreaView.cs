﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_EnemyAreaView : MonoBehaviour
{
    private scr_EnemyControllerAdri m_enemy;

    private void Awake()
    {
        m_enemy = gameObject.GetComponentInParent<scr_EnemyControllerAdri>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player" && m_enemy.actualState != scr_EnemyControllerAdri.EnemyStates.DYING)
        {
            m_enemy.setTarget(other.gameObject.transform);
            m_enemy.EnterAreaAction();
        }
            
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag.Equals("Player") && m_enemy.actualState != scr_EnemyControllerAdri.EnemyStates.DYING)
            m_enemy.ExitAreaAction();
    }
}