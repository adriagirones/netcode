﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_CollisionEnemy : MonoBehaviour
{
    public float damage;

    public bool isBoss;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            scr_PlayerController.instance.HittedBullet(isBoss);
        }
    }

}
