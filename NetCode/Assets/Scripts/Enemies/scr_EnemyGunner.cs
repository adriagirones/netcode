﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_EnemyGunner : scr_EnemyControllerAdri
{
    public float stopFollowingTime = 2f;
    public float offSetDistance = 2f;
    public float durationAttack = 2f;
    public scr_DaniBulletManager bulletManager;
    public Transform positionGun;

    #region DEACTIVATED
    protected override void OnStart_Deactivated()
    {
        anim.SetFloat("Blend", 0f);
    }
    #endregion

    #region FOLLOW_PLAYER
    protected override void OnStart_FollowPlayer()
    {
        anim.SetTrigger("running");
        anim.SetFloat("Blend", 1f);
    }
    protected override void Update_FollowPlayer()
    {
        agent.SetDestination(target.position);

        if (Vector3.Distance(target.transform.position, this.transform.position) <= this.GetComponentInChildren<SphereCollider>().radius + offSetDistance)
            changeState(EnemyStates.ATTACKING);
    }
    protected override void OnDisable_FollowPlayer()
    {
        anim.ResetTrigger("running");
    }
    #endregion

    #region ATTACK
    public float cooldownAttack = 3f;
    protected override void OnStart_Attacking()
    {
        agent.isStopped = true;
        anim.ResetTrigger("attackIdle");
        anim.SetTrigger("attack");
        StartCoroutine(AttackingTime());
    }
    protected override void Update_Attacking()
    {
        transform.LookAt(new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z));
    }
    protected override void OnDisable_Attacking()
    {
        agent.isStopped = false;
        StopAllCoroutines();
        //anim.SetTrigger("running");
        anim.ResetTrigger("attack");
        anim.ResetTrigger("attackIdle");
    }

    IEnumerator AttackingTime()
    {
        float timer = 0;
        while (timer < durationAttack)
        {         
            bulletManager.GetComponent<scr_DaniBulletManager>().startShooting(enemyInfo, positionGun);
            print("dispara");
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.shoot2);
            yield return new WaitForSecondsRealtime(durationAttack / enemyInfo.cadency);
            timer += (durationAttack / enemyInfo.cadency);
        }
        anim.SetTrigger("attackIdle");
        yield return new WaitForSecondsRealtime(cooldownAttack);

        changeState(EnemyStates.FOLLOW_PLAYER);
    }

    #endregion

    #region FOLLOW_BUT_NOT_SEEING
    private float actualStopFollowingTime = 0f;
    protected override void OnStart_FollowButNotSeeing()
    {
        anim.SetTrigger("running");
        actualStopFollowingTime = 0f;
    }
    protected override void Update_FollowButNotSeeing()
    {
        agent.SetDestination(target.position);
        actualStopFollowingTime += Time.deltaTime;
        if (actualStopFollowingTime >= stopFollowingTime)
        {
            changeState(EnemyStates.RETURN_INIT_POS);
        }
    }
    protected override void OnDisable_FollowButNotSeeing()
    {
        anim.ResetTrigger("running");
        actualStopFollowingTime = 0f;
    }
    #endregion

    #region RETURN_INIT_POST
    protected override void Update_ReturnInitPos()
    {
        agent.SetDestination(initPosition);

        if (Mathf.Approximately(initPosition.x, transform.position.x) && Mathf.Approximately(initPosition.z, transform.position.z))
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, initRotation, Time.deltaTime * 2);
            changeState(EnemyStates.DEACTIVATED);
            StartCoroutine(ToInitRotation());
        }
    }

    IEnumerator ToInitRotation()
    {
        float timeRotation = 3;
        while (timeRotation > 0)
        {
            if (actualState == EnemyStates.DEACTIVATED) transform.rotation = Quaternion.Lerp(transform.rotation, initRotation, Time.deltaTime * 2);
            timeRotation -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
    #endregion

    #region DYING
    protected override void OnStart_Dying()
    {
        anim.ResetTrigger("hitted");
        anim.ResetTrigger("running");
        anim.ResetTrigger("attack");
        anim.ResetTrigger("attackIdle");
        anim.SetTrigger("die");
        StartCoroutine(Die());
        transform.Find("EnemyHurtbox").GetComponent<CapsuleCollider>().enabled = false;
        agent.isStopped = true;
        transform.Find("PodCenter").transform.position = new Vector3(5000, 5000, 5000);
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(4f);
        scr_PlayerController.instance.player.skillPoints += 4;
        scr_PlayerController.instance.player.upExp(10);
        GameObject.Destroy(this.gameObject);
    }
    #endregion

    #region HITTED
    public float iFrames = 0.2f;
    private float iFramesTimer = 0f;
    public override void Hitted(float damage, bool isBullet)
    {
        if (!hpbar.activeSelf)
            hpbar.SetActive(true);
        if (iFramesTimer == 0)
        {
            hp -= damage;
            lerpTimer = 0;
            if (hp <= 0)
            {
                changeState(EnemyStates.DYING);
                hp = 0;
            }
            else if(!isBullet)
            {
                changeState(EnemyStates.HITTED);
            }
        }
    }

    protected override void OnStart_Hitted()
    {
        StopAllCoroutines();
        anim.SetTrigger("hitted");
        agent.isStopped = true;
    }

    protected override void Update_Hitted()
    {
        if (target != null)
            transform.LookAt(new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z));
        iFramesTimer += Time.deltaTime;
        if (iFramesTimer > iFrames)
            changeState(EnemyStates.FOLLOW_PLAYER);
    }

    protected override void OnDisable_Hitted()
    {
        iFramesTimer = 0;
        agent.isStopped = false;
    }
    #endregion

    public override void EnterAreaAction()
    {
        changeState(EnemyStates.FOLLOW_PLAYER);
    }

    public override void ExitAreaAction()
    {
        StopAllCoroutines();
        changeState(EnemyStates.FOLLOW_BUT_NOT_SEEING);
    }

}
