﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using Unity.Rendering;
using UnityEngine.Rendering.Universal;

public class scr_ConfigMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    public UniversalRenderPipelineAsset pipeline;

    public Dropdown resolutionDropdown;

    Resolution[] resolutions;

    void Start()
    {
        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> parsedres = new List<string>();
        int currentResIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string auxres = resolutions[i].width + " x " + resolutions[i].height;
            parsedres.Add(auxres);
            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResIndex = i;
            }
        }
        resolutionDropdown.AddOptions(parsedres);
        resolutionDropdown.value = currentResIndex;
        resolutionDropdown.RefreshShownValue();
    }

    public void OnChangeVolume(float vol)
    {
        audioMixer.SetFloat("volume", vol);
    }

    public void OnChangeQuality(int index)
    {
        QualitySettings.SetQualityLevel(index);
    }

    public void OnChangeFullscreen(bool fullscreen)
    {
        Screen.fullScreen = fullscreen;
    }

    public void OnChangeResolution(int index)
    {
        Resolution res = resolutions[index];
        Screen.SetResolution(res.width, res.height, Screen.fullScreen);
    }

    public void OnChangeAntialiasing(int index)
    {
        int auxval = 0;
        switch (index)
        {
            case 1:
                auxval = 2;
                break;
            case 2:
                auxval = 4;
                break;
            case 3:
                auxval = 8;
                break;
            default:
                auxval = 0;
                break;
        }
        //QualitySettings.antiAliasing = index;
        pipeline.msaaSampleCount = auxval;
    }

    public void OnChangeHDR(bool hdr)
    {
        pipeline.supportsHDR = hdr;
    }

    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        
    }
}
