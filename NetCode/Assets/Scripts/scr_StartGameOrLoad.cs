﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class scr_StartGameOrLoad : MonoBehaviour
{

    public so_Player myPlayerStats;
    public so_Checkpoint[] checkpoints;
    public so_Weapon[] weapons;
    public so_Nodes[] nodes;
    public scr_MenuManager menuManager;
    public so_savePipeline pipeline;
    public UniversalRenderPipelineAsset pipelineAsset;

    private BinaryFormatter bf = new BinaryFormatter();

    private void Awake()
    {
        scr_AudioManager.instance.playSound(scr_AudioManager.sound.intromusic);
        Time.timeScale = 1;
        if (File.Exists(Application.persistentDataPath + "/saveGames/statsPJ/character_save.json"))
        {
            FileStream file = File.Open(Application.persistentDataPath + "/saveGames/pipeline/pipeline_save.json", FileMode.Open);
            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), pipeline);
            file.Close();
            pipelineAsset.supportsHDR = pipeline.hdrActivated;
            pipelineAsset.msaaSampleCount = pipeline.antiAliassing;
            Screen.SetResolution(pipeline.resolutionWidth, pipeline.resolutionHeight, pipeline.fullscreen);
        }
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void StartGame()
    {
        //reiniciamos statsPJ
        myPlayerStats.maxhp = myPlayerStats.hpLvl1;
        myPlayerStats.podatk = myPlayerStats.podAtkLvl1;
        myPlayerStats.atk = myPlayerStats.atkLvl1;
        myPlayerStats.skillPoints = 0;
        myPlayerStats.xp = 0;
        myPlayerStats.lvl = 1;
        myPlayerStats.xpNextLevel = 5;
        //reiniciamos checkpoints
        for(int i = 0; i < checkpoints.Length; i++)
        {
            checkpoints[i].isActivated = false;
        }
        checkpoints[0].isActivated = true;
        //reiniciamos armas
        for (int i = 0; i < weapons.Length; i++)
        {
            weapons[i].isEnabled = false;
            weapons[i].isActived = false;
        }
        weapons[0].isEnabled = true;
        weapons[0].isActived = true;
        //nodes
        for (int i = 0; i < nodes.Length; i++)
        {
            nodes[i].activated = false;
        }
        nodes[0].activated = true;
        menuManager.OnSelectCheckpoint(0);
    }
    private void Update()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void LoadGame()
    {
        if (!Directory.Exists(Application.persistentDataPath + "/saveGames/statsPJ"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/saveGames/statsPJ");
        }
        if (File.Exists(Application.persistentDataPath + "/saveGames/statsPJ/character_save.json"))
        {
            FileStream file = File.Open(Application.persistentDataPath + "/saveGames/statsPJ/character_save.json", FileMode.Open);
            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), myPlayerStats);
            file.Close();
        }
        //checkpoints
        int lastCheckpointActivated = 0;
        for (int i = 0; i < checkpoints.Length; i++)
        {
            FileStream file = File.Open(Application.persistentDataPath + "/saveGames/checkpoints/checkpoints_save" + i + ".json", FileMode.Open);
            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), checkpoints[i]);
            file.Close();
            if (checkpoints[i].isActivated)
            {
                lastCheckpointActivated = i;
            }
        }
        menuManager.OnLoadGameCheckpoint(lastCheckpointActivated);

        for (int i = 0; i < weapons.Length; i++)
        {
            FileStream file = File.Open(Application.persistentDataPath + "/saveGames/weapons/weapons_save" + i + ".json", FileMode.Open);
            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), weapons[i]);
            //if (weapons[i].isActived)
                //menuManager.OnChangeWeapon(i);
            file.Close();
        }
        for (int i = 0; i < nodes.Length; i++)
        {
            FileStream file = File.Open(Application.persistentDataPath + "/saveGames/nodes/nodes_save" + i + ".json", FileMode.Open);
            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), nodes[i]);
            file.Close();
        }

    }
}
