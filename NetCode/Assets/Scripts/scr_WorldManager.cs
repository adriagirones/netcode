﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.UI;

public class scr_WorldManager : MonoBehaviour
{ 

    public InputController controls;
    public ChangeWorldEvent changeWorldEvent;
    public static scr_WorldManager instance;

    // Start is called before the first frame update
    private void Awake()
    {
        controls = new InputController();
        instance = this;
    }

    void Start()
    {
        controls.Gameplay.ChangeWorld.performed += ctx => OnChangeWorld();

        timerChangeWorld = maxTimerChangeWorld;
        waitTimeText.text = string.Format("{0:0.0}", timerChangeWorld);

        EnableWorldBlue();
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }

    public bool getIsInWorldBlue() {
        return isInWorldBlue;
    }


    #region CHANGEWORLD
    private bool isInWorldBlue = true;
    private bool interruptWorld = true;
    [Header("Parameters World Change")]
    public float maxTimerChangeWorld = 10f;
    private float timerChangeWorld = 0;
    public TextMeshProUGUI waitTimeText;
    public Image timerUI;

    [Header("Objects to Change Between Worlds")]
    public GameObject[] objectsWorldBlue;
    public GameObject[] objectsWorldRed;
    public Material skybox;
    public Material meshMaterial;
    [ColorUsage(true, true)]
    public Color blueGlow;
    [ColorUsage(true, true)]
    public Color redGlow;
    public MeshRenderer[] meshesToChangeMaterial;

    private void OnChangeWorld()
    {
        if(!scr_PlayerController.instance.isMenuEnabled)
            StartCoroutine(ChangeWorldIE());
    }

    IEnumerator ChangeWorldIE()
    {
        changeWorldEvent.Raise();
        if (isInWorldBlue)
        {
            interruptWorld = false;
            EnableWorldRed();
            while(!interruptWorld && timerChangeWorld > 0)
            {
                timerChangeWorld -= Time.deltaTime;
                if (timerChangeWorld < 0) timerChangeWorld = 0;
                waitTimeText.text = string.Format("{0:0.0}", timerChangeWorld);
                UpdateWorldTimerUI();
                yield return new WaitForEndOfFrame();
            }
            if(!interruptWorld)
                StartCoroutine(ChangeWorldIE());
        }
        else
        {
            interruptWorld = true;
            EnableWorldBlue();
            while (interruptWorld && timerChangeWorld < maxTimerChangeWorld)
            {
                timerChangeWorld += Time.deltaTime * 0.2f;
                if (timerChangeWorld > maxTimerChangeWorld) timerChangeWorld = maxTimerChangeWorld;
                waitTimeText.text = string.Format("{0:0.0}", timerChangeWorld);
                UpdateWorldTimerUI();
                yield return new WaitForEndOfFrame();
            }
        }
    }

    private void UpdateWorldTimerUI()
    {
        timerUI.fillAmount = timerChangeWorld / maxTimerChangeWorld;
    }

    private void EnableWorldBlue()
    {
        print("WORLD CHANGED TO BLUE");
        isInWorldBlue = true;

        foreach (GameObject go in objectsWorldBlue)
            go.SetActive(true);

        foreach (GameObject go in objectsWorldRed)
            go.SetActive(false);

        meshMaterial.SetColor("Color_64DB3A19", blueGlow);

        skybox.SetColor("_Tint", new Color(0.5f, 0.5f, 0.5f));
    }

    private void EnableWorldRed()
    {
        print("WORLD CHANGED TO RED");
        isInWorldBlue = false;

        foreach (GameObject go in objectsWorldBlue)
            go.SetActive(false);

        foreach (GameObject go in objectsWorldRed)
            go.SetActive(true);

        meshMaterial.SetColor("Color_64DB3A19", redGlow);

        skybox.SetColor("_Tint", new Color(1f, 0f, 0f));
    }

    #endregion

    #region CHECKPOINTS
    [Header("Checkpoints")]
    public GameObject[] checkpoints;
    public Transform getLastCheckpoint()
    {
        GameObject lastCheckpoint = null;
        foreach(GameObject checkpoint in checkpoints)
        {
            if (checkpoint.GetComponent<scr_Checkpoint>().scriptableCheckpoint.isActivated)
                lastCheckpoint = checkpoint;
        }
        return lastCheckpoint.GetComponent<scr_Checkpoint>().spawn;
    }
    #endregion
}
