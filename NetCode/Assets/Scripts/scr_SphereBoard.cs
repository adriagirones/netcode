﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class scr_SphereBoard : MonoBehaviour
{
    public Button firstButton;
    public GameObject tablero;
    public Canvas myCanvas;
    public so_UnlockedNodes unlocked;
    private Vector3 goToButtPosition;
    private RectTransform tableroRect;
    private GameObject lastSphere;
    private GameObject nowSphere;
    // Start is called before the first frame update

    void Start()
    {
        firstButton.Select();
        centrarAlCanvas();
        paintUnlocked();
        lastSphere = EventSystem.current.currentSelectedGameObject;
    }

    private void Update()
    {
        goToSphere();
    }

    public void goToSphere()
    {
        nowSphere = EventSystem.current.currentSelectedGameObject;
        if (nowSphere != lastSphere) {
            centrarAlCanvas();
            if (EventSystem.current.currentSelectedGameObject != null) {
                goToButtPosition = EventSystem.current.currentSelectedGameObject.GetComponent<RectTransform>().localPosition;
                tableroRect.position = tableroRect.position - goToButtPosition;
                lastSphere = nowSphere;
            }

        }
        
    }

    private void centrarAlCanvas() {
        tableroRect = tablero.GetComponent<RectTransform>();
        tableroRect.position = myCanvas.transform.position;
      //  tableroRect.position = Camera.main.ScreenToWorldPoint(new Vector3((Screen.width / 2) - tableroRect.position.x, (Screen.height / 2) - tableroRect.position.y, tableroRect.position.z));
      //  tableroRect.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, tableroRect.position.z));
    }

    private void paintUnlocked()
    {
        if (nowSphere != null && nowSphere.GetComponent<scr_Sphere>().mySphere.activated)
        {
            //está activada, cambiale el sprite
        }
    }

    private void unlockSphere()
    {
        if(!nowSphere.GetComponent<scr_Sphere>().mySphere.activated)
        {
            nowSphere.GetComponent<scr_Sphere>().mySphere.activated = true;
            paintUnlocked();
        }
    }


}
