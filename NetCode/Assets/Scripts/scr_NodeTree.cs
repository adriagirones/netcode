﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class scr_NodeTree : MonoBehaviour
{
    public Button firstButton;
    public so_UnlockedNodes unlocked;
    public GameObject nodes;
    public TMPro.TextMeshProUGUI titleNode;
    public TMPro.TextMeshProUGUI textNode;
    public TMPro.TextMeshProUGUI pointsToUnlock;
    public TMPro.TextMeshProUGUI skillPointsText;
    public GameObject costPanel;
    public GameObject mainPJ;
    private GameObject nowSphere;
    private GameObject lastSphere;

    private void Start()
    {
        firstButton.Select();
        foreach(scr_Sphere node in nodes.transform.GetComponentsInChildren<scr_Sphere>())
        {
            if (node.mySphere.activated)
            {
                var colors = node.gameObject.GetComponent<Button>().colors;
                colors.normalColor = Color.blue;
                node.gameObject.GetComponent<Button>().colors = colors;
            }
            else
            {
                var colors = node.gameObject.GetComponent<Button>().colors;
                colors.normalColor = Color.red;
                node.gameObject.GetComponent<Button>().colors = colors;
            }
        }
        skillPointsText.text = scr_PlayerController.instance.player.skillPoints.ToString() + " Skill Points";
    }

    private void OnEnable()
    {
        skillPointsText.text = scr_PlayerController.instance.player.skillPoints.ToString() + " Skill Points";
    }

    private void Update()
    {
        goToSphere();
    }

    public void goToSphere()
    {
        nowSphere = EventSystem.current.currentSelectedGameObject;
        if (nowSphere != lastSphere)
        {
            if (EventSystem.current.currentSelectedGameObject != null && nowSphere.GetComponent<scr_Sphere>() != null)
            {
                titleNode.text = nowSphere.GetComponent<scr_Sphere>().mySphere.title;
                textNode.text =  nowSphere.GetComponent<scr_Sphere>().mySphere.description;
                if(nowSphere.GetComponent<scr_Sphere>().mySphere.activated == false)
                {
                    costPanel.SetActive(true);
                    pointsToUnlock.text = nowSphere.GetComponent<scr_Sphere>().mySphere.cost.ToString() + " Skill Points";
                }
                else
                {
                    costPanel.SetActive(false);
                }

                lastSphere = nowSphere;
            }
        }
    }


    public void unlockSphere(scr_Sphere node)
    {
        if (node.lastSphere != null && !node.mySphere.activated && node.lastSphere.mySphere.activated && node.mySphere.cost <= scr_PlayerController.instance.player.skillPoints)
        {
            node.mySphere.activated = true;
            var colors = node.gameObject.GetComponent<Button>().colors;
            colors.normalColor = Color.blue;
            node.gameObject.GetComponent<Button>().colors = colors;
            scr_PlayerController.instance.player.skillPoints -= node.mySphere.cost;
            skillPointsText.text = scr_PlayerController.instance.player.skillPoints.ToString() + " Skill Points";
        }
    }

    private void paintUnlocked()
    {
        if (nowSphere.GetComponent<scr_Sphere>() != null && nowSphere != null && nowSphere.GetComponent<scr_Sphere>().mySphere.activated)
        {
            //está activada, cambiale el sprite
        }
    }
}
