﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_TargetDetection : MonoBehaviour
{
    public List<Transform> targets = new List<Transform>();

    private void Update()
    {
        foreach (Transform t in targets)
            if (t == null)
                targetsToDelete.Add(t);

        foreach (Transform t in targetsToDelete)
            TargetState(t, false);
        targetsToDelete.Clear();
    }

    List<Transform> targetsToDelete = new List<Transform>();

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PodCenter"))
        {
            TargetState(other.transform, true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PodCenter"))
        {
            TargetState(other.transform, false);
        }
    }

    public void TargetState(Transform target, bool state)
    {
        if (!state && targets.Contains(target))
            targets.Remove(target);

        if (state && !targets.Contains(target))
            targets.Add(target);
    }

}
