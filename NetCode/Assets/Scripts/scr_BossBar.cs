﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class scr_BossBar : MonoBehaviour
{
    public float chipSpeed = 2f;
    public Image frontHealthBar;
    public Image backHealthBar;
    public scr_Boss boss;

    bool isCorrutineRunning;

    public scr_BossPart.Part part;

    private float auxhp = 0;
    private float auxmaxhp = 0;

    public so_Boss bossStats;

    public void UpdateHealthUI()
    {
        switch (part)
        {
            case scr_BossPart.Part.ARM:
                auxhp = boss.armhp;
                auxmaxhp = bossStats.maxarmhp;
                break;
            case scr_BossPart.Part.CANON:
                auxhp = boss.canonhp;
                auxmaxhp = bossStats.maxcanonhp;
                break;
            case scr_BossPart.Part.OVERHEAT:
                auxhp = boss.overheat;
                auxmaxhp = bossStats.overheat;
                break;
            default:
                auxhp = boss.generalhp;
                auxmaxhp = bossStats.maxarmhp + bossStats.maxcanonhp + bossStats.generalhp;
                break;
        }

        if (!isCorrutineRunning)
            StartCoroutine(UpdateHealthUIIE());
    }

    IEnumerator UpdateHealthUIIE()
    {
        isCorrutineRunning = true;
        float fillF = frontHealthBar.fillAmount;
        float fillB = backHealthBar.fillAmount;
        float hFraction = auxhp / auxmaxhp;
        //print(hFraction);

        while (fillB != hFraction || fillF != hFraction)
        {
            fillF = frontHealthBar.fillAmount;
            fillB = backHealthBar.fillAmount;
            hFraction = auxhp / auxmaxhp;
            if (fillB > hFraction)
            {
                frontHealthBar.fillAmount = hFraction;
                boss.lerpTimerHPbar += Time.deltaTime;
                float percentComplete = boss.lerpTimerHPbar / chipSpeed;
                percentComplete *= percentComplete;
                backHealthBar.fillAmount = Mathf.Lerp(fillB, hFraction, percentComplete);
            }
            if (fillF < hFraction)
            {
                backHealthBar.fillAmount = hFraction;
                boss.lerpTimerHPbar += Time.deltaTime;
                float percentComplete = boss.lerpTimerHPbar / chipSpeed;
                percentComplete *= percentComplete;
                frontHealthBar.fillAmount = Mathf.Lerp(fillF, backHealthBar.fillAmount, percentComplete);
            }
            yield return new WaitForEndOfFrame();
        }
        isCorrutineRunning = false;
    }

    public void RestartHP()
    {
        isCorrutineRunning = false;
        switch (part)
        {
            case scr_BossPart.Part.ARM:
                auxhp = boss.armhp;
                auxmaxhp = bossStats.maxarmhp;
                break;
            case scr_BossPart.Part.CANON:
                auxhp = boss.canonhp;
                auxmaxhp = bossStats.maxcanonhp;
                break;
            case scr_BossPart.Part.OVERHEAT:
                auxhp = boss.overheat;
                auxmaxhp = bossStats.overheat;
                break;
            default:
                auxhp = boss.generalhp;
                auxmaxhp = bossStats.maxarmhp + bossStats.maxcanonhp + bossStats.generalhp;
                break;
        }
        frontHealthBar.fillAmount = auxhp / auxmaxhp;
        backHealthBar.fillAmount = auxhp / auxmaxhp;
        boss.lerpTimerHPbar = 0;

    }
}
