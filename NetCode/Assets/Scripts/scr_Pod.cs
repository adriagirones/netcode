﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_Pod : MonoBehaviour
{
    public float velocity;
    public Transform positionPod;
    public GameObject player;

    public scr_TargetDetection targetDetection;

    public GameObject prefabBala;

    public Canvas canvas;

    public GameObject prefabTargetUI;

    public InputController controls;

    private void Awake()
    {
        controls = new InputController();

        controls.Gameplay.Shoot.performed += cntxt => StartShooting();
        controls.Gameplay.Shoot.canceled += cntxt => EndShooting();

        controls.Gameplay.ChangeTarget.performed += cntxt => changeTarget();
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }

    bool shooting;

    void StartShooting()
    {
        shooting = true;
        print("start");
    }

    void EndShooting()
    {
        shooting = false;
        print("end");
    }

    void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, positionPod.position, velocity * Time.deltaTime);
        if(actualTarget >= 0 && targetDetection.targets.Count > 0)
        {
            if(targetDetection.targets[actualTarget] != null)
            {
                Vector3 relativePos = targetDetection.targets[actualTarget].position - transform.position;
                Quaternion toRotation = Quaternion.LookRotation(relativePos);
                transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, 5f * Time.deltaTime);
            }
           
        }
        else
            transform.forward = Vector3.Lerp(transform.forward, player.transform.forward, velocity * Time.deltaTime);
        
    }

    private void Update()
    {
        
        if (!player.GetComponent<scr_PlayerController>().isMenuEnabled)
        {
            DestroyTarget();
            foreach (Transform t in targetDetection.targets)
            {
                if(t != null)
                    LockTarget(t);
            }
        }

        if (shooting && !cooldown)
        {
            cooldown = true;
            StartCoroutine(ShootIE());
        }
        
    }

    public void DestroyTarget()
    {
        scr_TargetUI[] locks = FindObjectsOfType<scr_TargetUI>();

        foreach (scr_TargetUI l in locks)
        {
            bool flag = false;
            foreach(Transform t in targetDetection.targets)
            {
                if (l.target == t)
                    flag = true;
            }
            if (!flag)
            {
                Destroy(l.gameObject);
                //actualTarget--;
            }
                
        }
        if (targetDetection.targets.Count <= actualTarget) actualTarget = targetDetection.targets.Count - 1;
    }

    public int actualTarget = -1;

    public Color red;
    public Color white;

    public void LockTarget(Transform target)
    {
        Vector3 targetScreenPos = Camera.main.WorldToScreenPoint(target.position);

        scr_TargetUI[] locks = FindObjectsOfType<scr_TargetUI>();

        foreach (scr_TargetUI l in locks)
        {
            if (l.target == targetDetection.targets[actualTarget])
                l.GetComponent<Image>().color = red;
            else
                l.GetComponent<Image>().color = white;
        }

        foreach (scr_TargetUI l in locks)
        {
            if (l.target == target)
                return;
        }

        GameObject lockIcon = Instantiate(prefabTargetUI, targetScreenPos, Quaternion.identity, canvas.transform);
        lockIcon.GetComponent<scr_TargetUI>().target = target;
        if (actualTarget == -1) actualTarget = 0;
    }

    public void changeTarget()
    {
        if(actualTarget > -1)
        {
            actualTarget++;
            if (actualTarget >= targetDetection.targets.Count) actualTarget = 0;
        }
    }

    bool cooldown;
    IEnumerator ShootIE()
    {
        Shoot();
        yield return new WaitForSeconds(0.1f);
        cooldown = false;
    }

    void Shoot()
    {
        if(targetDetection.targets.Count > 0)
        {
            GameObject go = scr_Pool.instance.GetObjFromPool();
            go.transform.position = this.transform.GetChild(1).position;
            go.GetComponent<scr_BulletPod>().velocity = 10f;
            go.GetComponent<scr_BulletPod>().target = targetDetection.targets[actualTarget].position;
        }
    }



}
