﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class scr_Checkpoint : MonoBehaviour
{
    public GameObject stone1;
    public GameObject stone2;
    public GameObject turret;
    public Transform spawn;
    public so_Checkpoint scriptableCheckpoint;
    public Material materialNoActivated;
    public Material materialActivated;
    public ParticleSystem particles;
    //el menu es necesario para modificar el lastCheckpointActivated
    public scr_MenuManager menu;

    // Start is called before the first frame update
    private void Awake()
    {
        scriptableCheckpoint.spawnPoint = spawn;
    }

    void Start()
    {
        ActivateCheckpoint();
    }

    // Update is called once per frame
    void Update()
    {
        stone1.transform.RotateAround(transform.position, transform.up, Time.deltaTime * 90f);
        stone2.transform.RotateAround(transform.position, transform.up, -Time.deltaTime * 90f);
    }

    void ActivateCheckpoint()
    {
        if (scriptableCheckpoint.isActivated)
        {
            stone1.GetComponent<MeshRenderer>().material = materialActivated;
            stone2.GetComponent<MeshRenderer>().material = materialActivated;
            Material[] matArray = turret.GetComponent<MeshRenderer>().materials;
            matArray[1] = materialActivated;
            turret.GetComponent<MeshRenderer>().materials = matArray;
        }
        else
        {
            stone1.GetComponent<MeshRenderer>().material = materialNoActivated;
            stone2.GetComponent<MeshRenderer>().material = materialNoActivated;
            Material[] matArray = turret.GetComponent<MeshRenderer>().materials;
            matArray[1] = materialNoActivated;
            turret.GetComponent<MeshRenderer>().materials = matArray;
        }
    }

    public scr_PlayerBar hpBarPlayer;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (!scriptableCheckpoint.isActivated)
            {
                scriptableCheckpoint.isActivated = true;
                particles.Play();
                ActivateCheckpoint();
            }
            scr_PlayerController.instance.lerpTimerHPbar = 0;
            scr_PlayerController.instance.player.hp = scr_PlayerController.instance.player.getGlobalMaxHP();
            hpBarPlayer.UpdateHealthUI();
        }
    }
}
