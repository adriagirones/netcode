﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scr_Boss : MonoBehaviour
{
    public so_Boss bossStats;
    public enum EnemyStates
    {
        DEACTIVATED,
        ACTIVATED,
        ATTACKING,
        ATTACKARM,
        ATTACKCANON,
        IDLE,
        HITTED,
        DYING
    }
    public Animator anim;
    public EnemyStates actualState;

    protected Vector3 initPosition;
    protected Quaternion initRotation;

    [HideInInspector]
    public float lerpTimer = 0f;
    //public GameObject hpbar;

    bool IsInWorldRed;
    float normalSpeed;

    public GameObject arm;
    public GameObject canon;
    public GameObject cockpitTarget;
    //public GameObject cpit;

    public float armhp;
    public float canonhp;
    public float generalhp;

    public GameObject armhpbar;
    public GameObject canonhpbar;
    public GameObject generalhpbar;
    public GameObject overheatbar;

    bool fistarm = true;
    bool canonarm = true;
    bool cockpit = true;

    float waitAttack = 0;
    public float overheat = 150;

    [HideInInspector]
    public float lerpTimerHPbar = 0;

    scr_BulletManager[] canons;

    private void Awake()
    {
        //armhp = 300;
        //canonhp = 200;
        //generalhp = generalhp + armhp + canonhp;
        //overheat = 150;
        armhp = bossStats.maxarmhp;
        canonhp = bossStats.maxcanonhp;
        overheat = bossStats.overheat;
        generalhp = bossStats.generalhp + armhp + canonhp;


        initPosition = this.transform.position;
        initRotation = this.transform.rotation;
        canons = transform.GetComponentsInChildren<scr_BulletManager>();
        foreach (var canon in canons)
        {
            canon.enabled = false;
        }
        armhpbar.SetActive(false);
        canonhpbar.SetActive(false);
    }

    private void Start()
    {
        //hpbar.SetActive(false);
        actualState = EnemyStates.ACTIVATED;
        OnStart_Activated();
    }

    private void Update()
    {
        print(actualState+" "+waitAttack);
        switch (actualState)
        {
            case EnemyStates.ACTIVATED:
                Update_Activated();
                break;
            case EnemyStates.DEACTIVATED:
                Update_Deactivated();
                break;
            case EnemyStates.IDLE:
                Update_Idle();
                break;
            case EnemyStates.ATTACKING:
                Update_Attacking();
                break;
            case EnemyStates.ATTACKARM:
                Update_AttackArm();
                break;
            case EnemyStates.ATTACKCANON:
                Update_AttackCanon();
                break;
            case EnemyStates.HITTED:
                Update_Hitted();
                break;
            case EnemyStates.DYING:
                Update_Dying();
                break;
        }
    }

    public void changeState(EnemyStates newState)
    {
        print("CHANGE STATE: " + newState);
        switch (actualState)
        {
            case EnemyStates.ACTIVATED:
                OnDisable_Activated();
                break;
            case EnemyStates.DEACTIVATED:
                OnDisable_Deactivated();
                break;
            case EnemyStates.IDLE:
                OnDisable_Idle();
                break;
            case EnemyStates.ATTACKING:
                OnDisable_Attacking();
                break;
            case EnemyStates.ATTACKARM:
                OnDisable_AttackArm();
                break;
            case EnemyStates.ATTACKCANON:
                OnDisable_AttackCanon();
                break;
            case EnemyStates.HITTED:
                OnDisable_Hitted();
                break;
            case EnemyStates.DYING:
                OnDisable_Dying();
                break;
        }
        switch (newState)
        {
            case EnemyStates.ACTIVATED:
                OnStart_Activated();
                break;
            case EnemyStates.DEACTIVATED:
                OnStart_Deactivated();
                break;
            case EnemyStates.IDLE:
                OnStart_Idle();
                break;
            case EnemyStates.ATTACKING:
                OnStart_Attacking();
                break;
            case EnemyStates.ATTACKARM:
                OnStart_AttackArm();
                break;
            case EnemyStates.ATTACKCANON:
                OnStart_AttackCanon();
                break;
            case EnemyStates.HITTED:
                OnStart_Hitted();
                break;
            case EnemyStates.DYING:
                OnStart_Dying();
                break;
        }
        actualState = newState;
        waitAttack = 0;
    }
    #region ACTIVATE
    protected void OnStart_Activated()
    {
        anim.SetTrigger("activate");
        scr_AudioManager.instance.playSound(scr_AudioManager.sound.powerOnBoss);
        overheat = bossStats.overheat;
    }
    protected void Update_Activated() {
        waitAttack += Time.deltaTime;
        if(waitAttack >= 2f)
            changeState(EnemyStates.ATTACKING);
    }
    protected void OnDisable_Activated(){
    }
    #endregion

    #region DEACTIVATE
    protected void OnStart_Deactivated() {
        anim.SetTrigger("deactivate");
        scr_AudioManager.instance.playSound(scr_AudioManager.sound.enemyFollowandBossState);
        waitAttack = 0;
    }
    protected void Update_Deactivated() {
        waitAttack += Time.deltaTime;
        if (waitAttack >= 3f && (canonarm || fistarm))
        {
            changeState(EnemyStates.ACTIVATED);
        }
    }
    protected void OnDisable_Deactivated() {
    }
    #endregion

    #region IDLE
    protected void OnStart_Idle() {
        if (canon)
        {
            foreach (var canon in canons)
            {
                canon.enabled = false;
            }
        }
        anim.SetTrigger("idle");
        scr_AudioManager.instance.playSound(scr_AudioManager.sound.enemyFollowandBossState);
    }
    protected void Update_Idle()
    {
        waitAttack += Time.deltaTime;
        if (overheat <= 0)
            changeState(EnemyStates.DEACTIVATED);
        else if (waitAttack >= 4f)
            changeState(EnemyStates.ATTACKING);
        

    }
    protected void OnDisable_Idle() { }
    #endregion

    #region ATTACK
    protected void OnStart_Attacking() {

    }
    protected void Update_Attacking() {

        waitAttack += Time.deltaTime;
        if (waitAttack >= 0.2f)
        {
            if (fistarm && !canonarm)
                changeState(EnemyStates.ATTACKARM);
            else if (!fistarm && canonarm)
                changeState(EnemyStates.ATTACKCANON);
            else if (!fistarm && !canonarm)
                changeState(EnemyStates.DEACTIVATED);
            else
            {
                if (Random.Range(0, 3) == 0)
                    changeState(EnemyStates.ATTACKARM);
                else
                    changeState(EnemyStates.ATTACKCANON);
            }
        }
    }
    protected void OnDisable_Attacking() { }


    protected void OnStart_AttackArm() {
        anim.SetTrigger("punch");
        scr_AudioManager.instance.playSound(scr_AudioManager.sound.powerOnBoss);
    }
    protected void Update_AttackArm()
    {
        waitAttack += Time.deltaTime;
        print(waitAttack);
        if (waitAttack >= 5f)
            changeState(EnemyStates.IDLE);
    }
    protected void OnDisable_AttackArm() {
        anim.SetTrigger("punchend");
    }
    private float betweenshots = 0;
    protected void OnStart_AttackCanon() {
        anim.SetTrigger("shoot");
        betweenshots = 0;
        foreach (scr_BulletManager canon in canons)
        {
            canon.enabled = true;
            canon.shootmode = scr_BulletManager.ShootModes.boss1;
        }
    }
    protected void Update_AttackCanon()
    {
        waitAttack += Time.deltaTime;
        betweenshots += Time.deltaTime;
        if (waitAttack >= 8f)
            changeState(EnemyStates.IDLE);
        else if (betweenshots >= 1f)
        {
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.bossShoot);
            foreach (scr_BulletManager canon in canons)
            {
                if(canon.shootmode == scr_BulletManager.ShootModes.boss2)
                    canon.shootmode = scr_BulletManager.ShootModes.boss1;
                else
                    canon.shootmode = scr_BulletManager.ShootModes.boss2;
            }
            betweenshots = 0;
        }

    }
    protected void OnDisable_AttackCanon() {
        foreach (var canon in canons)
            canon.enabled = false;
        anim.SetTrigger("shootend");
    }
    #endregion

    #region HITTED
    protected void OnStart_Hitted() {
        anim.SetTrigger("hit");
        scr_AudioManager.instance.playSound(scr_AudioManager.sound.bosshitted);
    }
    protected void Update_Hitted()
    {
        if ((fistarm || canonarm) && cockpit)
            changeState(EnemyStates.IDLE);
        else if (overheat <= 0 && cockpit)
            changeState(EnemyStates.DEACTIVATED);
        
    }
    protected void OnDisable_Hitted() { }

    #endregion

    #region DYING
    protected void OnStart_Dying() {
        if(actualState != EnemyStates.DYING) {
            anim.SetTrigger("die");
            scr_AudioManager.instance.playSound(scr_AudioManager.sound.bossdie);
            StartCoroutine(destroyGameObject(cockpitTarget));
            armhp = 0;
            canonhp = 0;
            overheat = 0;
            if (canon)
            {
                foreach (var canon in canons)
                {
                    canon.enabled = false;
                }
            }
            armhpbar.GetComponent<scr_BossBar>().UpdateHealthUI();
            canonhpbar.GetComponent<scr_BossBar>().UpdateHealthUI();
        }
       
    }
    protected void Update_Dying() {
        waitAttack += Time.deltaTime;
        if (waitAttack >= 4f)
        {
            this.GetComponent<scr_SaveGame>().SaveGame();
            SceneManager.LoadScene("StartScene");
            armhpbar.SetActive(false);
            canonhpbar.SetActive(false);
            overheatbar.SetActive(false);
            generalhpbar.SetActive(false);
            gameObject.SetActive(false);
            
        }
    }
    protected void OnDisable_Dying() { }
    #endregion

    public void Hitted(float damage, scr_BossPart.Part part)
    {
        switch (part){
            case scr_BossPart.Part.ARM:
                if (!armhpbar.activeSelf)
                    armhpbar.SetActive(true);
                armhp -= damage;
                armhpbar.GetComponent<scr_BossBar>().UpdateHealthUI();
                break;
            case scr_BossPart.Part.CANON:
                if (!canonhpbar.activeSelf)
                    canonhpbar.SetActive(true);
                canonhp -= damage;
                canonhpbar.GetComponent<scr_BossBar>().UpdateHealthUI();
                break;
        }
        generalhp -= damage;
        generalhpbar.GetComponent<scr_BossBar>().UpdateHealthUI();
        overheat -= damage / 2;
        overheatbar.GetComponent<scr_BossBar>().UpdateHealthUI();

        if (generalhp <= 0 && generalhp >= -700)
        {
            cockpit = false;
            //cockpitTarget.SetActive(false);
            changeState(EnemyStates.DYING);
        }
        else if (armhp <= 0 && armhp >= -700)
        {
            changeState(EnemyStates.HITTED);
            fistarm = false;
            StartCoroutine(destroyGameObject(arm));
            armhp = -999;
        }
        else if (canonhp <= 0 && canonhp >= -700)
        {
            changeState(EnemyStates.HITTED);
            canonarm = false;
            StartCoroutine(destroyGameObject(canon));
            canonhp = -999;
        }
        else if (overheat <= 0)
            changeState(EnemyStates.DEACTIVATED);
    }

    IEnumerator destroyGameObject(GameObject go)
    {
        if (go) 
        {
            go.transform.position = new Vector3(5000, 5000, 5000);
            yield return new WaitForEndOfFrame();
            GameObject.Destroy(go);
        }
   
    }

    public void OnChangeWorld()
    {
        IsInWorldRed = !IsInWorldRed;
        //if (IsInWorldRed)
        //    agent.speed = normalSpeed / 3;
        //else
        //    agent.speed = normalSpeed;

    }
}
