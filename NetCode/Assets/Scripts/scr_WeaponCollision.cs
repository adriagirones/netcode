﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_WeaponCollision : MonoBehaviour
{
    public float damage;
    public bool isBullet;

    public enum typeWeapon
    {
        SWORD,
        SKILLFIRE,
        SKILLWIND,
        SKILLICE,
        SKILLELECT,
        SKILLELECTBALL,
        BULLET
    }

    public typeWeapon type;

    private void Start()
    {
        switch (type)
        {
            case typeWeapon.SWORD:
                damage = scr_PlayerController.instance.player.getGlobalAtk();
                break;
            case typeWeapon.SKILLFIRE:
                damage = scr_PlayerController.instance.player.skillsUnlocked.getDamageFireSkill();
                break;
            case typeWeapon.SKILLWIND:
                damage = scr_PlayerController.instance.player.skillsUnlocked.getDamageWindSkill();
                break;
            case typeWeapon.SKILLICE:
                damage = scr_PlayerController.instance.player.skillsUnlocked.getDamageIceSkill();
                break;
            case typeWeapon.SKILLELECT:
                damage = scr_PlayerController.instance.player.skillsUnlocked.getDamageElectSkill();
                break;
           case typeWeapon.SKILLELECTBALL:
                damage = scr_PlayerController.instance.player.skillsUnlocked.getDamageElectSkillBall();
                break;
            case typeWeapon.BULLET:
                damage = scr_PlayerController.instance.player.getGlobalAtkPod();
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            scr_PlayerController.instance.HitEnemy(other.gameObject, damage, isBullet);
        }
    }
  
}
