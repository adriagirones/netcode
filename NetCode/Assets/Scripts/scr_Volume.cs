﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class scr_Volume : MonoBehaviour
{
    Volume volume;
    bool isWorldBlue;


    private void Awake()
    {
        volume = GetComponent<Volume>();
        
    }

    void Start()
    {
        volume.enabled = false;
        isWorldBlue = true;
    }

    public void changeWorld()
    {
        isWorldBlue = !isWorldBlue;
        if (isWorldBlue)
            volume.enabled = false;
        else
            volume.enabled = true;
    }

    public void InMenu(bool flag)
    {
        volume.enabled = flag;
    }
}
