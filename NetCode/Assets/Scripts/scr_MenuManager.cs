﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class scr_MenuManager : MonoBehaviour
{
    public GameObject CheckpointMenu;
    public GameObject MainMenu;
    public GameObject StatsMenu;
    public GameObject ConfigMenu;
    public GameObject SkillsMenu;
    public GameObject SwordsMenu;
    public GameObject NodeMenu;
    public GameObject myPlayer;

    public void BackToTheMenu()
    {
        CheckpointMenu.SetActive(false);
        StatsMenu.SetActive(false);
        ConfigMenu.SetActive(false);
        SkillsMenu.SetActive(false);
        SwordsMenu.SetActive(false);
        NodeMenu.SetActive(false);
        MainMenu.SetActive(true);
    }

    #region CHECKPOINTMENU

    public so_Checkpoint[] checkpointsSO;
    [HideInInspector]
    public int lastCheckpointIndex = 0;
    public GameObject[] checkpointUI;

    public void EnableCheckpointMenu()
    {
        MainMenu.SetActive(false);
        CheckpointMenu.SetActive(true);
        if(checkpointsSO[1].isActivated) 
            checkpointUI[0].transform.GetChild(1).gameObject.SetActive(false);
        if (checkpointsSO[2].isActivated)
            checkpointUI[1].transform.GetChild(1).gameObject.SetActive(false);
    }

    public void DisableCheckpointMenu()
    {
        CheckpointMenu.SetActive(false);
        MainMenu.SetActive(true);
    }

    public void OnSelectCheckpoint(int index)
    {
        print("OnSelectCheckpoint" + index);
        if (checkpointsSO[index].isActivated)
        {
            lastCheckpointIndex = index;
            print("OnSelectCheckpoint IS ACTIVATED! TE HAGO TP");
            if (checkpointsSO[index].sceneName != SceneManager.GetActiveScene().name)
            {
                //DisableCheckpointMenu();
                //scr_PlayerController.instance.player.positionLastCheckpoint = checkpointsSO[index].nameGameObject;
                StartCoroutine(LoadAsync(index));
                print("SI LLEGAAAA");
                //scr_PlayerController.instance.transform.position = checkpointsSO[index].spawnPoint.position;
            }
            else
            {
                scr_PlayerController.instance.transform.position = checkpointsSO[index].spawnPoint.position;
                //scr_PlayerController.instance.transform.position = scr_WorldManager.instance.getLastCheckpoint().position;
                DisableCheckpointMenu();
                scr_PlayerController.instance.OpenMainMenu();
            }
        }
    }

    public void OnLoadGameCheckpoint(int index)
    {
        print("OnSelectCheckpoint" + index);
        if (checkpointsSO[index].isActivated)
        {
            lastCheckpointIndex = index;
            print("OnSelectCheckpoint IS ACTIVATED! TE HAGO TP");
            if (checkpointsSO[index].sceneName != SceneManager.GetActiveScene().name)
            {
                //DisableCheckpointMenu();
                //scr_PlayerController.instance.player.positionLastCheckpoint = checkpointsSO[index].nameGameObject;
                StartCoroutine(LoadAsync(index));
                print("SI LLEGAAAA");
                //scr_PlayerController.instance.transform.position = checkpointsSO[index].spawnPoint.position;
            }
            else
            {
                //scr_PlayerController.instance.transform.position = checkpointsSO[index].spawnPoint.position;
                scr_PlayerController.instance.transform.position = scr_WorldManager.instance.getLastCheckpoint().position;
                DisableCheckpointMenu();
                scr_PlayerController.instance.OpenMainMenu();
            }
        }
    }

    IEnumerator LoadAsync(int indexCheckpoint)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(checkpointsSO[indexCheckpoint].sceneName);

        while (!operation.isDone)
        {
            yield return null;
        }
        scr_PlayerController.instance.transform.position = scr_WorldManager.instance.getLastCheckpoint().position;
        //scr_PlayerController.instance.transform.position = checkpointsSO[indexCheckpoint].spawnPoint.position;
        //scr_PlayerController.instance.OpenMainMenu();s
    }

    #endregion

    #region STATSMENU

    public TMP_Text statsText;
    public void EnbaleStatsMenu()
    {
        MainMenu.SetActive(false);
        StatsMenu.SetActive(true);
        statsText.text = myPlayer.GetComponent<scr_PlayerController>().player.lvl + "\n"
            + myPlayer.GetComponent<scr_PlayerController>().player.maxhp + "\n"
            + myPlayer.GetComponent<scr_PlayerController>().player.atk + "\n"
            + myPlayer.GetComponent<scr_PlayerController>().player.getGlobalAtkPod();
        //ataque del pod

    }

    public void DisableStatsMenu()
    {
        StatsMenu.SetActive(false);
        MainMenu.SetActive(true);
    }

    #endregion

    #region WEAPONMENU

    public Material[] colors;
    public MeshRenderer[] meshesMaterials;
    public GameObject[] weapons;
    public so_Weapon[] weaponsSO;

    public void EnbaleWeaponsMenu()
    {
        MainMenu.SetActive(false);
        SwordsMenu.SetActive(true);
    }

    public void DisableWeaponsMenu()
    {
        SwordsMenu.SetActive(false);
        MainMenu.SetActive(true);
    }

    public void OnSelectColor(int numColor)
    {
        foreach(MeshRenderer mesh in meshesMaterials)
        {
            Material[] materials = mesh.materials;
            materials[0] = colors[numColor];
            mesh.materials = materials;
        }
    }

    public void OnChangeWeapon(int index)
    {
        int auxIndex = 0;
        if (!weaponsSO[index].isEnabled)
        {
            foreach(so_Weapon w in weaponsSO)
            {
                if (w.isActived)
                {
                    w.isActived = false;
                    weapons[auxIndex].SetActive(false);
                }
                auxIndex++;
            }
            weapons[index].SetActive(true);
            weaponsSO[index].isActived = true;
            weaponsSO[index].isEnabled = true;
            
        }
        else if (weaponsSO[index].isEnabled)
        {
            foreach (so_Weapon w in weaponsSO)
            {
                if (w.isActived)
                {
                    w.isActived = false;
                    weapons[auxIndex].SetActive(false);
                }
                auxIndex++;
            }
            weapons[index].SetActive(true);
            weaponsSO[index].isActived = true;
        }
    }

    #endregion

    #region NODETREEMENU

    public void EnbaleNodeMenu()
    {
        MainMenu.SetActive(false);
        NodeMenu.SetActive(true);
    }

    public void DisableNodeMenu()
    {
        NodeMenu.SetActive(false);
        MainMenu.SetActive(true);
    }
    #endregion

    #region CONFIGMENU

    public void EnableConfigMenu()
    {
        MainMenu.SetActive(false);
        ConfigMenu.SetActive(true);
    }

    public void DisableConfigMenu()
    {
        ConfigMenu.SetActive(false);
        MainMenu.SetActive(true);
    }
    #endregion

    #region SKILLSMENU

    public void EnableSkillsMenu()
    {
        MainMenu.SetActive(false);
        SkillsMenu.SetActive(true);
    }

    public void DisableSkillsMenu()
    {
        SkillsMenu.SetActive(false);
        MainMenu.SetActive(true);
    }
    #endregion

    #region EXITMENU

    public void exitMenu()
    {
        SceneManager.LoadScene("StartScene");
    }

    #endregion
}
