﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_BulletPod : MonoBehaviour
{
    public float velocity;
    public Vector3 target;

    private void Update()
    {
        transform.LookAt(target);
        this.GetComponent<Rigidbody>().velocity = velocity * transform.forward;
        if(Vector3.Distance(target, transform.position) < 0.1f)
            scr_Pool.instance.ReturnObjToPool(this.gameObject);
    }
}
