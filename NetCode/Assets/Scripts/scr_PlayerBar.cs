﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class scr_PlayerBar : MonoBehaviour
{
    public float chipSpeed = 2f;
    public Image frontHealthBar;
    public Image backHealthBar;
    public scr_PlayerController mainCharacter;

    bool isCorrutineRunning;

    public void UpdateHealthUI()
    {
        if (!isCorrutineRunning)
            StartCoroutine(UpdateHealthUIIE());
    }

    IEnumerator UpdateHealthUIIE()
    {
        isCorrutineRunning = true;
        float fillF = frontHealthBar.fillAmount;
        float fillB = backHealthBar.fillAmount;
        float hFraction = mainCharacter.player.hp / mainCharacter.player.getGlobalMaxHP();
        //print(hFraction);

        while (fillB != hFraction || fillF != hFraction)
        {
            fillF = frontHealthBar.fillAmount;
            fillB = backHealthBar.fillAmount;
            hFraction = mainCharacter.player.hp / mainCharacter.player.getGlobalMaxHP();
            if (fillB > hFraction)
            {
                frontHealthBar.fillAmount = hFraction;
                mainCharacter.lerpTimerHPbar += Time.deltaTime;
                float percentComplete = mainCharacter.lerpTimerHPbar / chipSpeed;
                percentComplete *= percentComplete;
                backHealthBar.fillAmount = Mathf.Lerp(fillB, hFraction, percentComplete);
            }
            if (fillF < hFraction)
            {
                backHealthBar.fillAmount = hFraction;
                mainCharacter.lerpTimerHPbar += Time.deltaTime;
                float percentComplete = mainCharacter.lerpTimerHPbar / chipSpeed;
                percentComplete *= percentComplete;
                frontHealthBar.fillAmount = Mathf.Lerp(fillF, backHealthBar.fillAmount, percentComplete);
            }
            yield return new WaitForEndOfFrame();
        }
        isCorrutineRunning = false;
    }

    public void RestartHP()
    {
        isCorrutineRunning = false;
        frontHealthBar.fillAmount = mainCharacter.player.hp / mainCharacter.player.getGlobalMaxHP();
        backHealthBar.fillAmount = mainCharacter.player.hp / mainCharacter.player.getGlobalMaxHP();
        mainCharacter.lerpTimerHPbar = 0;

    }
}
