﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class scr_SaveGame : MonoBehaviour
{
    public so_Player myPlayerStats;
    public so_Checkpoint[] checkpoints;
    public so_Weapon[] weapons;
    public so_Nodes[] nodes;
    public so_savePipeline pipeline;
    [HideInInspector]
    private BinaryFormatter bf = new BinaryFormatter();

    public bool isDirectoryCreated()
    {
        return Directory.Exists(Application.persistentDataPath + "/saveGames");
    }

    public void SaveGame()
    {
        //comprobamos que directorios hace falta crear
        if (!isDirectoryCreated())
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/saveGames");
            Directory.CreateDirectory(Application.persistentDataPath + "/saveGames/statsPJ");
            Directory.CreateDirectory(Application.persistentDataPath + "/saveGames/checkpoints");
            Directory.CreateDirectory(Application.persistentDataPath + "/saveGames/weapons");
            Directory.CreateDirectory(Application.persistentDataPath + "/saveGames/nodes");
            Directory.CreateDirectory(Application.persistentDataPath + "/saveGames/pipeline");
        }
        print(Application.persistentDataPath + "/saveGames");
        //create files
        FileStream filePJ = File.Create(Application.persistentDataPath + "/saveGames/statsPJ/character_save.json");
        FileStream fileLastCheckpoint = File.Create(Application.persistentDataPath + "/saveGames/checkpoints/last_checkpoint_save.json");

        //creates json
        var jsonStats = JsonUtility.ToJson(myPlayerStats);


        //serialize and save
        bf.Serialize(filePJ, jsonStats);

        //file closes
        filePJ.Close();
        fileLastCheckpoint.Close();


        //checkpoints
        for (int i = 0; i < checkpoints.Length; i++)
        {
            FileStream fileCheckpoints = File.Create(Application.persistentDataPath + "/saveGames/checkpoints/checkpoints_save" + i +".json");
            var jsonCheckpoints = JsonUtility.ToJson(checkpoints[i]);
            bf.Serialize(fileCheckpoints, jsonCheckpoints);
            fileCheckpoints.Close();
        }
        //weapons
        for(int i = 0; i < weapons.Length; i++)
        {
            FileStream fileWeapons = File.Create(Application.persistentDataPath + "/saveGames/weapons/weapons_save" + i + ".json");
            var jsonWeapons = JsonUtility.ToJson(weapons[i]);
            bf.Serialize(fileWeapons, jsonWeapons);
            fileWeapons.Close();
        }
        //nodes
        for(int i = 0; i < nodes.Length; i++)
        {
            FileStream fileNodes = File.Create(Application.persistentDataPath + "/saveGames/nodes/nodes_save" + i + ".json");
            var jsonNodes = JsonUtility.ToJson(nodes[i]);
            bf.Serialize(fileNodes, jsonNodes);
            fileNodes.Close();
        }
        //pipeline
        pipeline.hdrActivated = pipeline.pipeline.supportsHDR;
        pipeline.antiAliassing = pipeline.pipeline.msaaSampleCount;
        pipeline.resolutionHeight = Screen.height;
        pipeline.resolutionWidth = Screen.width;
        pipeline.fullscreen  = Screen.fullScreen;
        FileStream filePipe = File.Create(Application.persistentDataPath + "/saveGames/pipeline/pipeline_save.json");
        var jsonPipeline = JsonUtility.ToJson(pipeline);
        bf.Serialize(filePipe, jsonPipeline);
        filePipe.Close();

    }
}
