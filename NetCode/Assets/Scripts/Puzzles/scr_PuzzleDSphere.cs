﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_PuzzleDSphere : MonoBehaviour
{
    public Transform destination;
    public float velocity = 5f;
    public string color;
    public Material normalColor;
    public Material realColor;

    private void Start()
    {
        OnChangeColor(true);
    }

    private void FixedUpdate()
    {
        if(destination != null)
            transform.position = Vector3.Lerp(transform.position, destination.position, velocity * Time.deltaTime);
    }

    public void OnChangeColor(bool isWorldBlue)
    {
        if (isWorldBlue)
            GetComponent<MeshRenderer>().material = normalColor;
        else
            GetComponent<MeshRenderer>().material = realColor;
    }
}
