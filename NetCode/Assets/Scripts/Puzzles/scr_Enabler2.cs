﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Enabler2 : MonoBehaviour
{
    public Material newMaterial;
    public bool isEnabled = false;
    public scr_Platform platform;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("weapon"))
        {
            if (!isEnabled)
            {
                isEnabled = true;
                this.transform.GetChild(0).GetComponent<MeshRenderer>().material = newMaterial;
                platform.canStart();
            }
        }
    }
}
