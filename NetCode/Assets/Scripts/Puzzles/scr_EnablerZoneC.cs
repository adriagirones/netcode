﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_EnablerZoneC : MonoBehaviour
{
    public bool isRightMechanism;

    public bool isInteractuable = false;
    bool activated = false;

    public scr_ZoneCManager managerC;

    public Material enabledMaterial;
    public Material interactuableMaterial;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("weapon") && isInteractuable)
        {
            if (!activated)
            {
                activated = true;
                if (isRightMechanism) 
                    managerC.ActivatePuzzle3();
                else 
                    managerC.ActivatePuzzle4();

                transform.GetChild(0).GetComponent<MeshRenderer>().material = enabledMaterial;
            }
        }
    }

    public void OnInteractuable()
    {
        if (!activated) 
        {
            isInteractuable = true;
            transform.GetChild(0).GetComponent<MeshRenderer>().material = interactuableMaterial;
        }
    }
}
