﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Platform : MonoBehaviour
{
    public GameObject player;
    public GameObject platform;

    [Header("Animation")]
    public Transform position1;
    public Transform position2;
    public float time;
    public AnimationCurve animationCurve;

    public scr_Enabler2 mechanism1;
    public scr_Enabler2 mechanism2;


    private bool flag;
    void Start()
    {
        platform.transform.position = position1.position;
        canStart();
    }

    public void canStart()
    {
        if (mechanism1.isEnabled && mechanism2.isEnabled)
        {
            MoveTo(position2.position, time);
            flag = false;
        }
    }

    IEnumerator AnimateMove(Vector3 origin, Vector3 target, float duration)
    {
        float journey = 0f;
        while (journey <= duration)
        {
            journey = journey + Time.deltaTime;
            float percent = Mathf.Clamp01(journey / duration);

            float curvePercent = animationCurve.Evaluate(percent);
            platform.transform.position = Vector3.LerpUnclamped(origin, target, curvePercent);

            yield return null;
        }

        if (flag)
            MoveTo(position2.position, duration);
        else
            MoveTo(position1.position, duration);

        flag = !flag;
    }

    void MoveTo(Vector3 target, float duration)
    {
        StartCoroutine(AnimateMove(platform.transform.position, target, duration));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == player)
        {
            player.transform.parent = transform;
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject == player)
        {
            player.transform.parent = null;
        }
    }

    private void OnEnable()
    {
        platform.transform.position = position1.position;
        canStart();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

}
