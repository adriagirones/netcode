﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_PuzzleB : MonoBehaviour
{
    public scr_PuzzleBTurret[] turrets;

    public GameObject door;

    public void isPuzzleFinished()
    {
        bool correct = true;
        foreach (scr_PuzzleBTurret turret in turrets)
        {
            if (!turret.isEnabled) correct = false; 
        }

        if (correct)
        {
            door.SetActive(false);
        }
    }


}
