﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class scr_PuzzleBTurret : MonoBehaviour
{
    public bool isEnabled;

    public scr_PuzzleBTurret rightTurret;
    public scr_PuzzleBTurret leftTurret;

    public Material green;
    public Material yellow;

    private void Start()
    {
        EnableTurret(isEnabled);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "weapon")
        {
            EnableTurret(!isEnabled);
            rightTurret.EnableTurret(!rightTurret.isEnabled);
            leftTurret.EnableTurret(!leftTurret.isEnabled);
            transform.GetComponentInParent<scr_PuzzleB>().isPuzzleFinished();
        }
    }

    public void EnableTurret(bool enable)
    {
        isEnabled = enable;
        if (enable)
        {
            transform.GetComponentInChildren<VisualEffect>().Play();
            Material[] mat = transform.GetChild(1).GetComponent<MeshRenderer>().materials;
            mat[1] = green;
            transform.GetChild(1).GetComponent<MeshRenderer>().materials = mat;
        }
        else
        {
            transform.GetComponentInChildren<VisualEffect>().Stop();
            Material[] mat = transform.GetChild(1).GetComponent<MeshRenderer>().materials;
            mat[1] = yellow;
            transform.GetChild(1).GetComponent<MeshRenderer>().materials = mat;
        }
    }
}
