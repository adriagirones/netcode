﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_PuzzleC : MonoBehaviour
{
    private bool isInWorldBlue;
    public scr_WorldManager wm;
    public MeshRenderer[] platformsToActivate;
    public Material worldRed;
    public Material worldBlue;

    void Start()
    {
        isInWorldBlue = true;
        foreach (MeshRenderer m in platformsToActivate)
            m.material = worldBlue;
    }

    public void OnChangeWorld()
    {
        isInWorldBlue = !isInWorldBlue;
        if (isInWorldBlue)
            foreach (MeshRenderer m in platformsToActivate)
                m.material = worldBlue;
        else
            foreach (MeshRenderer m in platformsToActivate)
                m.material = worldRed;
    }
}
