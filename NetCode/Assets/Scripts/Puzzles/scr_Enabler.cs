﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Enabler : MonoBehaviour
{
    public GameObject door;
    public Material newMaterial;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("weapon"))
        {
            if (door.activeSelf)
            {
                door.SetActive(false);
                this.transform.GetChild(0).GetComponent<MeshRenderer>().material = newMaterial;
            }
        }
    }
}
