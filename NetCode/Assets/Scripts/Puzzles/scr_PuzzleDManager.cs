﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_PuzzleDManager : MonoBehaviour
{
    private bool isInWorldBlue;

    public scr_PuzzleDSphere[] spheres;
    public scr_PuzzleDMechanism[] mechanisms;

    public scr_EnablerZoneC enabler;

    private void Start()
    {
        isInWorldBlue = true;
    }

    public void OnChangeWorld()
    {
        isInWorldBlue = !isInWorldBlue;
        foreach (scr_PuzzleDSphere s in spheres)
            s.OnChangeColor(isInWorldBlue);
    }

    public void checkIfCorrect()
    {
        bool isAllCorrect = true;
        foreach(scr_PuzzleDMechanism m in mechanisms)
        {
            if (!m.isCorrect) isAllCorrect = false; 
        }

        if (isAllCorrect)
        {
            enabler.OnInteractuable();
        }
       
    }


}
