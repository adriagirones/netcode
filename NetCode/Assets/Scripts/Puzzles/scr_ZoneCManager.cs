﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_ZoneCManager : MonoBehaviour
{
    public MeshRenderer[] meshesPuzzle3;
    public MeshRenderer[] meshesPuzzle4;

    public bool puzzle3completed = false;
    public bool puzzle4completed = false;

    public Material white;
    public Material green;

    public GameObject door1;
    public GameObject door2;

    private void Start()
    {
        foreach(MeshRenderer m in meshesPuzzle3)
            m.material = white;

        foreach (MeshRenderer m in meshesPuzzle4)
            m.material = white;
    }

    public void ActivatePuzzle3()
    {
        puzzle3completed = true;
        foreach (MeshRenderer m in meshesPuzzle3)
            m.material = green;

        if(puzzle3completed && puzzle4completed)
        {
            door1.SetActive(false);
            door2.SetActive(false);
        }
    }

    public void ActivatePuzzle4()
    {
        puzzle4completed = true;
        foreach (MeshRenderer m in meshesPuzzle4)
            m.material = green;

        if (puzzle3completed && puzzle4completed)
        {
            door1.SetActive(false);
            door2.SetActive(false);
        }
    }
}
