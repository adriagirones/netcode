﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_PuzzleDMechanism : MonoBehaviour
{
    public bool sphereActive = true;
    public Transform spherePos;
    public GameObject sphere;

    public string color;

    public Material trueMaterial;
    public Material normalMaterial;

    bool isWorldBlue;

    public bool isCorrect;

    public scr_PuzzleDManager manager;

    private void Start()
    {
        if (sphereActive)
        {
            sphere.GetComponent<scr_PuzzleDSphere>().destination = spherePos;
        }
        isWorldBlue = true;
        isCorrect = false;
    }

    public void changeWorld()
    {
        isWorldBlue = !isWorldBlue;
        if (isWorldBlue)
            GetComponent<MeshRenderer>().material = normalMaterial;
        else
            GetComponent<MeshRenderer>().material = trueMaterial;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "weapon")
        {
            print("entra");
            if (sphere != null && scr_PlayerController.instance.gameObject.transform.Find("SphereSpawn").childCount == 0)
            {
                sphere.transform.parent = scr_PlayerController.instance.gameObject.transform.Find("SphereSpawn");
                sphere.GetComponent<scr_PuzzleDSphere>().destination = scr_PlayerController.instance.gameObject.transform.Find("SphereSpawn");
                sphere = null;
                sphereActive = false;
                isCorrect = false;
            }
            else
            {
                if (scr_PlayerController.instance.gameObject.transform.Find("SphereSpawn").childCount == 1 && sphere == null)
                {
                    sphere = scr_PlayerController.instance.gameObject.transform.Find("SphereSpawn").GetChild(0).gameObject;
                    scr_PlayerController.instance.gameObject.transform.Find("SphereSpawn").GetChild(0).transform.parent = transform;
                    sphere.transform.parent = transform;
                    sphere.GetComponent<scr_PuzzleDSphere>().destination = spherePos;
                    sphereActive = true;
                    if (sphere.GetComponent<scr_PuzzleDSphere>().color == color) isCorrect = true;
                    if(manager != null) manager.checkIfCorrect();
                }
            }
        }
    }
}
